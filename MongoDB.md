![logo](./assets/mongodb-logo.png)

        C'est un système de gestion de base de données orienté documuent
        Le package Mongoose permet de connecter l'application NodeJs avec MongoDB

![mean stack](./assets/mongodb-mean-stack.png)
![mean stack meaning](./assets/mongodb-mean-stack-1.png)

**MongoDB** est plus populaire des **systèmes de gestion de base de données** de type "**orienté document** ", dont le **langage** de commande natif **Javascript**, et surtout le format de données **BSON** est en correspondance directe avec la notion **JSON** qui est élément essentiel de la syntaxe de Javascript                
Cette adéquation immédiate entre le langage de programmation applicatif et le moteur d'accès aux données permet d'**éliminer la problématique de conversion entre les objets du web et les tables de la base de données** et de se **passer de la lourdeur traditionnelle des ORM**

➡️ **MongoDB** est un **SGBD** dit **nosql**

        NoSQL : (les bases de données) ont un schéma dynamique pour les données non structurées donc ne sont pas relationnelles → magasin de documents (clé valeur, graphique, colonne large)

---

## Mongoose

        Est un package JavaScript, Permet de nous connecter à l'application NodeJS avec une base donnée MongoDB

--- 

## Installation

- Étape 1 : https://www.mongodb.com/fr
- Étape 2 : Docs → Server
- Étape 3 : Installation 
- Étape 4 : Install MongoBD Community Edition
- Étape 5 : Install on macOs
- Étape 6 : copie / colle  `xcode-select --install`
- Étape 7 : Installation de Homebrew : copie / colle `/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"`  
- Étape 8 : copie / colle `brew tap mongodb/brew`
- Étape 9 : Vérification de l'installation : copie / colle `brew tap mongodb/brew`
- Étape 10 : Installation MongoDB version Community  : copie / colle `brew install mongodb-community@4.4` 

Les fichiers qu'on va avoir lors de l'installation :           
![fichier installation](./assets/mongodb-fichier.png)

Les dossiers qu'on va avoir lors de l'installation :             
![dossiers installation](./assets/mongodb-dossiers.png)

- Lancer MongoDB : `brew services start mongodb-community@4.4`
- Arrêter MongoDB : `brew services stop mongodb-community@4.4`
- Lancer manuellement : `mongod --config /usr/local/etc/mongod.conf --fork`
- Vérification du lancement : `brew services list`
- Vérification de la connexion : `mongo`

--- 

## Préparation d'une nouvelle API



