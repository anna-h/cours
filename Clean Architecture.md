![clean-architecture-logo](./assets/clean-architecture-logo.png)

    La clean architecture vise à réduire les dépendances avec les services (API, Base de données, Framework, Librairies tierces), pour maintenir une application stable au cours de l'évolution (lors de changement / mise à jour des ressources externes)

![schema](./assets/clean-architecture-schema.png)
![schema](./assets/clean-architecture-schema-1.png)

⬆️ On distingue une découpe de différentes couches (1er schéma) . Plus on s'approche du centre plus le logiciel devient de haut niveau. 
Les **couches internes** ne doivent pas être conscientes des **couches externes** = ne **pas** être **impactées**

![mvc](./assets/clean-architecture-mvc.png)

➡️ **MVC** : **M**odel **V**iew **C**ontroller 
→ C'est d'implémenter dans la plupart des projets ≠ pas très complexe + permet de bien **segmenter les couches applicatives**

→ **Controller** : est de maître + charge de capter la requête (la traite tout en servant des données fournies par le modèle => pour injecter les données voulues dans la vue)

- ❗️Lorsqu'on implémente une clean architecture❗️ (règles)
    - Etre indépendante des **frameworks** (framework & librairies = doivent être des outils)
    - Etre **testable** indépendamment (interface utilisateur, base de données ...)
    - Etre indépendante de l'**interface utilisateur** (console, interface web ...)
    - Etre indépendante de la **base de données** (SGBD)
    - Etre indépendante de **tout service / système externe** (pas avoir de conscience de ce qui l'entoure)
    
[☞ Lien explicative de Clean Architecture](https://blog.adimeo.com/forum-php-2019-clean-architecture)

