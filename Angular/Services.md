# LES SERVICES ET L'INJECTION DE DÉPENDANCE

    Un service : c'est une classe qui peut être utilisée par plusieurs composants → faire des appels HTTP, serveur, sortir un code + le rendre utilisable dans plusieurs composants, manager des données

`Injectable` : c'est un décorateur pour les classes, il sera une racine (faire une marque à la classe) pour qu'elle fournisse aussi dans d'autre composant (réutilisable)

- TS
```ts
//fichier auth.services.ts
import{Injectable} from  '@angular/core';

//Injecte à la classe (peut l'utiliser à plusieurs endroits)
@Injectable({
    providedIn: 'root' //data donnée , va être fourni à la racine de l'application
})
//⭕️Créer un service(classe)
export class AuthServices{
    
}
```

```ts
//fichier users.service.ts
import {Injectable} from '@angular/core';

//c'est un décorateur
@Injectable({
    providedIn: 'root'
})
export class UsersService {

    constructor() {
    }
}
```

```ts
//fichier users.component.ts
@Component({selector: 'app-users'...})
export class UsersComponent implements OnInit {

    userSelected: IUser;

    users: IUser[] = [...];
    
    //⭕️Permet d'injecter une instance
    constructor(private usersServices: UsersServices) { //déclare une variable + assigne une valeur → à besoin de dépendance(paramètre) =typage
    }
    
    ngOnInit(){
    }
    
    public setUserSelected(user: IUSer): void  {...}
    
    public setUserSelected(user: IUser): void {...}
}
```

[Lien utile (learn angular)](https://www.learn-angular.fr/injection-de-dependances-angular/)    
[Lien utile (UTM)](http://pierreterrat.com/dependances-angular/)
