![Angular](../assets/angular-logo.png)

---

# SOMMAIRE
- [Installation](#installation)
- [Présentation de la structure](#prsentation-de-la-structure)
- [Installer Bootstrap](#installer-boostrap)
- [Premier Composant](#premier-composant)
- [Interpolation Property Binding](#interpolation-property-binding)
- [Event Binding](#event-binding)
- [Two Way Data Binding](#two-way-data-binding)
- [Structural Directive ngIf](#structural-directive--ngif)
- [Structural Directive ngFor](#structural-directive-ngfor)
- [Partage de données entre les directives et composants enfants & parents](#partage-de-donnes-entre-les-directives-et-composants-enfants--parents)
  - [Input Decorator](#input-decorator)
  - [Output Decorator](#output-decorator)
- [Arrow function | le mot clé "this" | l'opérateur new](#arrow-function---mot-cl-this---oprateur-new)  
---
☞ [Lien Angular](https://angular.io/) ❇️

    Angular est un framwork et permet de créer des applications web dynamiques + immensives
   
``single page application`` = 
1. est principalement composé de JS, va être exécuté par le côté client le serveur va nous renvoyer du Js + c'est ce navigateur qui va exécuter ce Js là
2. C'est une en **une seule page**, contient **du HTML + Javascript**
3. va être en liaison avec le navigateur avec "http" (HyperText Transfer Protocol Secure) 

- **Pourquoi Angular est si intéressant ? (Projet d'application web)** 
    - CRM
    - Interface
    - Gestion compte bancaire
    - Gestion de facture
    - Interaction
---
## INSTALLATION 

---

- **ÉTAPE 1**  
 Aller sur le [Lien Angular](https://angular.io/)
- **ÉTAPE 2**  
  Recopier ``sudo npm install -g @angular/cli`` dans la terminal
- **ÉTAPE 3**    
  Créer une application avec ``ng``,
  Va afficher toutes commandes possibles
- **ÉTAPE 4**   
  - Créer première application ``ng new `` + le nom du dossier   
  (_Exemple_ : ng new my-app)
  - Demande si veux une "routing"    
    Réponse NON ❌ : réponse par défaut, appuyer sur entrée  
    Réponse OUI ✅ : mettre un "y" puis appuyer sur entrer
- **ÉTAPE 5**   
Choisir le format du style
  - CSS
  - SCSS
  - Sass
  - Less
  - Stylus
- **ÉTAPE 6**  
 Pour accéder au dossier utiliser la commande ``cd`` + le nom du dossier    
  - Va ajouter un dossier (ici sous le nom "my-app")     
  ![my-app](../assets/angular-setup.png)      
- **ÉTAPE 7**       
Lancer le serveur ``ng serve``
  
---
## PRÉSENTATION DE LA STRUCTURE

---

``ctrl + c dans la terminal`` + ``ng serve`` = arrêter le processus qui bloque la terminal (/ou (+) "local")

⬆️ **Image ci-dessus** ⬆️

️☞ Dossier   
⭕️ **e2e** : "End to End" , contient des tests → écrit coté navigateur  = qui va directement tester le HTML donc sur le navigateur   
⭕️ **src** : "source" , note tout le projet   
⭕️ **app** : Faire tous les changements/ modifications de l'application   

☞ Fichier  
⭕️ **angular.json** : fichier qui contient des informations sur notre projet  (architecture, structure, compilation, style, etc..)    
⭕️ **package.json** : va voir tous les dépendances + le script 


---
## INSTALLER BOOSTRAP 

---
☞ [Boostrap téléchargement](https://getbootstrap.com/docs/5.0/getting-started/download/)

➡️**INSTALLATION**
- **ÉTAPE 1** : Copier code npm ``npm install bootstrap@next`` 
- **ÉTAPE 2** : Coller sur la terminal 
- **ÉTAPE 3** : Vérifier dans fichier "package.json" → "dependencies" / dans dossier "node-modules" → dossier "bootstrap"

➡️**INSÉRER FICHIER STYLES BOOTSTRAP**
- **ÉTAPE 1** : Ouvrir fichier "angular.json"
- **ÉTAPE 2** : Dans ``"styles":`` écrire ``node_modules/bootstrap/dist/css/bootstrap.min.css``
- **ÉTAPE 3** : Rafraichir la page avec ``ctrl + c`` + ``ng serve`` dans la terminal

---
## PREMIER COMPOSANT

---

![app](../assets/angular-app.png) =  Dossier de composant 

1️⃣ ❕ **CRÉER UN NOUVEAU COMPOSANT** ️  (_exemple_) (le 2e app est un nouveau composant)
- **ÉTAPE 1** : Créer dossier "component" dans le dossier "app"
- **ÉTAPE 2** : Créer dossier "app" dans le dossier "component"
- **ÉTAPE 3** : Déplacer tous les fichiers du 1er dossier "app" au 2e dossier "app"

Comme ceci en dessous ⬇️     
![app2](../assets/angular-app-2.png)

**Composant** = C'est **une classe** qu'on **exporte** qui a noté une directive qui s'appelle **"@Component"** → directive est **importé** du corps d'angular " **'@angular/core'** "

⬇️**app.component.ts** (un des fichiers dans app)⬇️
````ts
import { Component } from '@angular/core';

@Component({ //DÉCORATEUR
  
  //❗️PARAMÈTRE → objets : 3 propriétes de bases ❗️
  selector: 'app-root',
  
  //va afficher comme HTML, affiche le mot "HELLO" en titre
  template: `` 
        <h1>HELLO</h1> 
  ``,
  //SI code HTML trop long → déplace dans le fichier dédié que du HTML
  templateUrl: './app.component.html',
  
  // écrire du css dans le fichier dédié au style (ici scss)
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'my-app';
}
````

⭕️ **DÉCLARE UN COMPOSANT**  

    Doit déclarer le nouveau composant créer ≠ sinon ne peut pas ne manipuler 

- **ÉTAPE 1** : Aller fichier "app.module.ts"
- **ÉTAPE 2** : Dans la propriété "declarations:" écrire le nouveau nom du composant 
- **ÉTAPE 3** : Importer ``import {nomDuNouveauComponent} from './components/nomDuNouveauCompenent/nomDuNouveauCompenent.component';``

⚠️ Ne pas oublier **import** + **export** dans le nouveau component pour pouvoir importe dans "app.module.ts" ⚠️

2️⃣ ❕ **CRÉER NOUVEAU COMPOSANT**

-  ➡️ Aller dans la terminal tape :``ng generate component`` / ``ng generate c`` / ``ng g c`` (même commande ≠ dans différente façon) + **nom du composant**
❗️ Déclare automatiquement dans app.module ❗️
---
## INTERPOLATION PROPERTY BINDING

---

⭕️ **INTERPOLATION** =  permet t'afficher une variable dans le HTML et Property Binding
**interpolation** `{{}}`= permet d'afficher une donnée dans le HTML depuis le typescript 

- HTML
````html
<h1>{{Title}}</h1>

<h2>{{getTitle()}}</h2>
````

- TS
````ts
export class AppComponent{
    //affiche ce contenu dans le HTML 
    Title: string = "Interpolation et Property Binding"
  
  getTitle(): string{ //Créer une nouvelle fonction
        return this.Title;
  }
}
````

**RÉSULTAT** (ce qu'affiche HTML)     
![ipb](../assets/angular-ipb.png)

⭕️ **PROPERTY BINDING** : Lier une propriété / attribut à une variable

``[innerText]="nomdelavariable"`` : lier à une variable 
``[innerHTML]="nomdelavariable`` : lier à une variable avec du code HTML dans angular sans interpréter les éléments HTML à l'intérieur

- HTML
````html
<h2 [innerText]="title"></h2>

➡️ ❗️VA JUSTE LIER À UNE VARIABLE❗️ (donc on voit la propriété dans le HTML<a></a>)


<h2 [innerHTML]="title"></h2>

➡️ ❗VA AFFICHER SANS LA PROPRIÉTÉ HTML IL LES PREND EN COMPTE 

````




- TS
````ts
export class AppComponent {
    title: string = "<a>Interpolation et Property Binding</a>"
  
  getTitle(): string{
        return .this.title;
  }
}
````

**RÉSULTAT** (ce qu'affiche HTML)    
![ipb2](../assets/angular-ipb-2.png)

----

## EVENT BINDING

    "Liaison d'évènement" : Permet d'écouter + répondre aux actions de l'utilisateur (frappres au clavier, mouvement de souris, les clics, les touches)

--- 

 ⭕️ **POUR BOUTON** ️   
``<button (click)="onSave()">Save</button>``: SYNTAXE DE LIAISON : syntaxe HTML, mettre des parenthèses dans l'évènement dans la propriété HTML + `="nomdelafonction()"`      
![eb-a-retenir](../assets/angular-eb-1.png)

- HTML 
````html
<button (click)="changeColor"></button> // ❗️ Écoute l'évènement "click" sur le propriété HTML "button", avec la fonction "changeColor" ❗️
````

- TS
````ts
export class AppComponent{
    title: string = "Event Binding";
    
    //DÉCLARE LA FONCTION
    public changeColor(){
        alert("Bouton cliqué")
    }
}
````


- **RÉSULTAT** :     
![eb](../assets/angular-eb.png)

 ⭕️ **INPUT** 

→ Référence = Identifiant 

- HTML
````html
<input #inputRef (change)="setText($event, inputRef)" type="text"></input> // Écoute un évènement en mettant des parenthèses dans la propriété HTML
//veut avoir accès à mon élément "change"
<p>{{ text }}</p>
````

`#inputRef` : Est une variable qui sera une référence à mon élément "input" , cette référence peut passer en paramètre dans la méthode (change)="setText()"     
`console.dir();` : Pour afficher toutes les propriétés correspondant dans la console
`$event` = récupérer les informations de mon élément (grâce à angular)

- TS
````ts
export class AppComponent{
    title: string = "Event Binding";
    text: string = '';
    
    public setText(input, event){
        this.text= input.value;
        console.log(input,event);
        
        //POUR AFFICHE TOUTES LES PROPRIÉTES DANS LA CONSOLE 
      console.dir(input.value);
    }
}
````

**RÉSULTAT** ( ce qu'il affiche : quand ajoute / modifie mon **"input"** → va aussi ajouter/ modifier la **variable** "mon texte" qui est interpellé dans mon **template** )   
![input](../assets/angular-input.png)

---

##  TWO WAY DATA BINDING

---
    C'est la liaison de données à double sens



⭕️ **LIAISON À 1 SENS**

- HTML 
````html
<input #inputRef (change)='setText(inputRef, $event)' type="text"></input> 

<button (click)="changeColor($event)">Click</button>

<p [style.color]="text">{{ text }}</p> // = Liaison à 1 sens
// la valeur va mettre dans la propriété [style color]
````

→ Écouter le changement de "**input**" + modifier le text grâce à la méthode "**setText**" dans laquelle donne la référence "**inputRef**"    
→ Dans méthode "**setText**" va récupérer la référence "**input**" + sa valeur ( grâce à "this"), donc va l'intégrer dans variable "**text**"  
→ La valeur "**text**" va venir la lier à la propriété "**color du style**" de l'élément "**p**" → donc il va afficher dans "**text**" 

- TS 
````ts
export class AppComponent {
   title: string = "Two Way data binding";
   text: tring = '';
   
   public changeColor(event){
       this.text = "Two way data binding Cliqué";
       console.log(event);
   }
    
    
  public setText(input, event){ 
    this.text = input.value;
  }
}

````

**⭕️ LIAISON À DOUBLE SENS**


☞ **SYNTAXE** (dans l'ordre, quand retrouve bindings)  
**property binding** `[]` = Lier des attributs à une variable     
**event binding** `()`= Écouter des évènements  
**two ways binding** `[()]` = Synchronisation complète d'une information entre composant parent + enfant    
**interpolation** `{{}}` = permet d'afficher une donnée dans le HTML depuis le typescript       
`[(ngModel)]` : Permet de faire two ways data binding 

- HTML 
````html
<input [(ngModel)]type="text"> 

<p [style.color]="text">{{ text }}</p>
````

- TS
````ts
export class AppComponent{
    title: string = "Two Way data binding";
    title: string= "";
}
````

→ Lier propriété "**text**" à "**input**"   
→ Valeur de la variable "**text**"
→ **[(ngModel)]** lie à propriété de la valeur de '**input**' à la variable '**text**' + à chaque modification → modifie la variable '**text**'    
→ Faire une importation dans le fichier **app.module.ts**        
![twdb](../assets/angular-twdb.png)

**À RETENIR** :
- 1er liaison : Lier la valeur de '**input**' à la valeur de la variable '**text**'
- 2ᵉ liaison : Modification la valeur de '**input**' + modifie la valeur de la variable '**text**' (changer de couleur en fonction du nom de la couleur donnée)      
![angular-twdb-green](../assets/angular-twdb-green.png)
![angular-twdb-red](../assets/anguler-twdb-red.png)  

---

## STRUCTURAL DIRECTIVE : ngIf

---

    Permet d'écrire des conditions dans note DOM pour supprimer / ajouter des éléments
`*ngIf` = mot clé + une condition (true/ false)

- HTML 
````html
<p *ngIf="true">{{ user.name }}</p>   
````
→ Ajouter une condition pour supprimer le '**user.name**' dans le paragraphe (si ma condition est '**true**' → donc '**p**' va être affiché)          
→ ✅ **true** = élément va être **ajouter** ≠ ❌ **false** = élément va être supprimé (vaut mieux supprimer pour qu'il soit +/ plus performant)         



- TS
````ts
export class AppComponent{
    title: string = "Structural Directive : ngIf";
    user: { name: string} = {name: "Anna"};
    show: boolean = false;
    
    isShown(): boolean{
        return this.show;
    }
}
````

 **⭕️ SAUVEGARDER DES DONNÉES**
- HTML 
````html
<p *ngIf="user; else userIsNull">{{ user }}</p> // ici ajoute ("else" = sinon) 

//SI ma condition est vrai, va afficher paragraphe ≠ SINON le référence 'userIsNull'
// VRAI ✅ va afficher name "Anna"
// FAUX ❌ va afficher référence "userIsNull"


<ng-template #userIsNul> // ('#': référence) variable qui penche vers HTML 
  <p>User est null</p>
</ng-template>
````
→ Tant que template est '**null**' '**user**' n'existe pas = ERROR   
→ Si faux ❌ donc différent de '**null**' = va afficher (là va rien afficher car la valeur est **nul** mais sera sans ERROR dans console)     
- TS
`````ts
export class AppComponent{
    title: string = "Structural Directive : ngIf";
    user: { name: string } = null; 
    show: boolean = true;
    
    isShown(): boolean{
        reurn this.show;
    }
}
`````
----
## STRUCTURAL DIRECTIVE ngFor

----

    Permet de faire des répétitions de le DOM 

- HTML
````html
//UTILISATION DE ngFor
// Ajoute la directive 'ngFor' dans élément 'p'
<p *ngFor="let user of user"> //Déclare la variable + contient la valeur du tableau 'user'
  {{ user.name }} // afficher le 'user.name'
</p> 

<button (click)="show = !show">Toggle tittle</button>

````



- TS
````ts
export class AppComponent{
    title: string = 'Structural Directive : ngFor';
    show: boolean = false;
    
    users: {
        name: string, color?: string } [] = [ // contient "tableaux d'objets" → va avoir une propriété 'name' + 'option color' (?:)
      {name: "Jean", color: "red"},
      {name: "Eva"},
      {name: "Sam", color: "blue"},
    ]; 
}
````

⭕️ **ACCÈS À L'INDEX**

`id` : donne la position

- HTML
````html
<p *ngFor="let user of users; let id = index">
  {{ id }} - {{ user.name }} // va afficher la valeur de l'index 
</p> 
````

→ Pour avoir accès à l'**index** → mettre point virgule **";"** après l'itération, `<p *ngFor="let user of users;>` (dans le premier paragraphe)    
→ Après avoir accès à l'**index** → peut ➕ ajouter une autre variable nommé '**id**' → qui prendra la valeur de nommée **index**         
![ngfor](../assets/angular-ngfor.png)


⭕️ **AJOUTER / SUPPRIMER DES ÉLÉMENTS**

- **Ajouter des boutons** 

- HTML 
````html
<div *ngFor="let user of users; let id =index">
  <div>{{ id }} - {{ user.name }}</div>
  
  <button (click)="showAlert(user)">Afficher</button> // Quand appuyer sur le bouton 'Afficher' → exécute 'showAlerte'
    // veut afficher 'user' (donc le 'name') quand appuie le bouton 'Afficher'
</div>
````

- TS
````ts
export class AppComponent {
    title: string = 'Structural Directive : ngFor';
    show: boolean = false;
    
    users: { name: string, color?: string} [] = [
      {name: 'Jean', color: 'grey'},
      {name: 'Eva'},
      {name: 'Sam', color: 'red'},
    ];
    
    showAlert(user): void{
        console.log(user);
    }
}
````

**RÉSULTAT**               
![ngFor](../assets/angular-ngfor-1.png)

- **Supprimer des éléments**

- HTML
````html
<div *ngFor="let user of users; let id = index">
    <div>{{ id }} - {{ user.name }}</div>
  
  <button (click)="showAlert(user,id)">Afficher</button>
</div>

<button (click)="show = !show">Toggle tittle</button>
````

- TS 
`````ts
showAlert(user, id): void{
    let arr = [...this.users]; // Créer un nouveau tableau , qui va récupérer toutes valeurs de 'user'
    arr.splice(id, 1); // supprimer des éléments à la position 'id' + supprimer 1 élément
    this.users = [...arr]; // mettre à jour tableau 'users' avec un nouveau tableau
  
  console.log(id);
}
`````
**RÉSULTAT**           
![ngfor](../assets/angular-ngfor-2.png)
 

⭕️ **LIER ngIf + ngFor**

`ng-container` = Regrouper les éléments qui n'interfère pas avec les styles / mises en pages , car Angular ne pas mets dans le DOM (contient du HTML)

- HTML
````html
<ng-container *ngFor="let user of users; let id = index"> // ❗️En faisant ça : Pour chaque élément de mon tableau , va créer une variable 'user' + dupliquer le container → va avoir plusieurs container à chaque fois ❗️ 
  <div *ngIf="user.color"> // ❗️Dans le container , va ajouter la directive 'ngIf' + veut qu'il m'affiche tout ce qu'il est contient dedans si 'user.color' = 'null'❗️
    
    <div>{{ id }} - {{ user.name }} | {{ user.color}} </div>

    <button (click)="deleteUser(id)">Afficher</button>
  </div>
</ng-container>

````

- TS
````ts
export class AppComponent{
  title : string = 'Structural Directive : ngFor'
  show: boolean = false;
  
  users: { name: string, color?:string} [] = [
    {name: 'Jean', color: ''}
  ]
}
````

❗️IMPORTANT❗️ : si utilise **ngFor** + **ngIf** faut faire une boucle → utilise **ng-container** ou/ (une **div**) : Pour les séparer + Afficher l'élément si elle respecte **certaine condition** 

----

## PARTAGE DE DONNÉES ENTRE LES DIRECTIVES ET COMPOSANTS ENFANTS & PARENTS

---

![input-output](../assets/angular-input-output.png)


## INPUT DECORATOR

    Le <parent-component> sert de contexte pour le <child-component>
    @Input() + 'composant enfant' (moyen de communiquer avec 'composant parent')  → Permet à un composant parent de mettre à jour les données du composant enfant 
    ≠ Permet aussi à l'enfant d'envoyer des données à un 'composant parent'

`@Input` : syntaxe, = 'entrée'

````ts
<parent-component>
    <child-component></child-component>
</parent-component>
````

![input](../assets/angular-input-1.png)

- TS 
````ts
//fichier 'task.component.ts'
import {Component, Input} from "@angular/core"; // + ajoute 'Input' ppur importer

@Component({
  selector: 'app-task',
  template: `<div class="task"> {{ id }} - {{ task }}</div>`,
  styleUrls: ['./task.component.scss']
})

export class TaskComponent{
    // Pour recevoir les données du parent → utilise @ (un décorateur)
    // Peut ajouter plusieurs 'Input' = 'entrée'
    @Input('index') // Donne un nom à l'input
    id: number; // propriété 'id'
  
    
    @Input() task: string;  // cette variable doit passer/ récupérer par le parent 
    

}
````

- HTML 
````html
// fichier 'app.component.html'
<div class="container">
  <h1 class="my-5">{{ title }}</h1>
  
  //COMPOSANT PARENT = 'app'
  
  // COMPOSANT ENFANT 
  <app-app-task></app-app-task>
  
  // COMPOSANT ENFANT   (donner la tâche 'task')
  //❗️ créer une structure répétitive d'un composant en passant 'task' + 'index' ❗️
  <app-task *ngFor="let task of tasks; let id = index" //tableau de chaîne de caractère 'task'
  
  //maintenant le 'composant' accepte 'input' la propriété 'task' ('[task]') → peut donner une variable ="task"
        [task]="task" [index]="id"></app-task> //faire une 'entrée' ( déclare les 'input' du fichier .ts)
  
  
  <button type="button" class="btn btn primary"(click)="save()">Enregistrer</button>
</div>
````
---

## OUTPUT DECORATOR

---

    @Output() propriété dans composant enfant, 'composant enfant' → (✈️ voyage les données) 'composant aprent'
    Composant enfant utilise la propriété pour déclencher un évènement → pour notidier la modification au parent
    Déclencher un évènement : type → qui a une classe pour émettre des évènements personnalisés @Output()@Ouput()EventEmitter@angular/core

`observable` : 


![output](../assets/angular-output.png)

- TS
```ts
//fichier 'app.task.component.ts'
import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.scss']
})

export class AddTaskComponent{
    text: string = '';
    
    //va avoir des sorties
    @Output('task') taskEmit = new EventEmitter();  // va avoir une nouvelle instance de 'EventEmitter'  
    // ❌ la sortie va pas êtr une valeur  ≠ → mais va être un 'observable' 
    // ❗️ quand veut émettre une donnée, on va les mettre dans un flux de données ❗️
    // ➡️ va renvoyer une donnée (output)
  
    addTask(): void{
        this.taskEmit.emit() // va renvoyer une chaîne de caractères → ça sera 'this.text'
    // + va émettre la variable 'task' avec le mot clé 'emit()'
    }
}
```

````ts
//fichier 'app.component.ts'
✅EXPLICATION✅
addTask(): void {      //prends rien an paramètre 
    this.tasks.push(this.text); // le contenu de la variable 'text' → push dans tableau 'tasks'
    this.text= ''; // récupère la variable 'text'
}

//✅ce qu'il faut faire✅(ici)
addTask(task): void{ // récupère la 'task'
    this.tasks.push(task); // + push 'task' dans tableau 'tasks'
}

````


- HTML
````html
//fichier 'app.component.html'
<div class="container">
  <h1 class="my-5">{{ title }}</h1>
  // va renvoyer une donnée ($event) va placer en argument la méthode, ☞alors que  l'évènement 'task' va être envoyé avec les données → cette donnée va donner en argument + la méthode 'addTask'
  <app-add-task (task)="addTask($event)"></app-add-task> // le nom d'élément écouter 'task'
  
  <app-task *ngFor="let task of tasks; let id = index" [task]="task" [index]="id"></app-task>

  <button type="button" class="btn btn-primary" (click)="save()">Enregistrer</button>

</div>
````
**RÉSULTAT**      
![output](../assets/angular-output-1.png)

⭕️ **SUPPRIMER DES ÉLÉMENTS**
````html
<div class="container">
  <h1 class="my-5">{{ title }}</h1>
  <app-add-task (task)="addTask($event)"></app-add-task>
  <app-task *ngFor="let task of tasks; let id= index">
    [task]="task" [index]="id"
    (click)="deleteTask(id)" // ❗️ va supprimer un élément à chaque qu'on appuie 'click' ❗️
  </app-task>
  
  <button type="button" class="btn btn)primary" (click)="save()">Enregistrer</button>
</div>
````

**RÉSULTAT** (à supprimer 2 éléments)
![output-2](../assets/angular-output-2.png)


---
## ARROW FUNCTION /  MOT CLÉ "THIS" /  OPÉRATEUR "NEW"

---

    Constructor function : Va créer objet vide '{}' grâce au 'new' +  va être lier par 'this' (camel case) ⚠️ 'this' va faire référence à un objet⚠️
    ☞ Permet de renvoyer une instance de la classe qu'on vient de nommer
    Factory function : Permet de créer des objets  (pascale case) + va renvoyé 'this' → 'window' global ⚠️ 'this' va faire référence à une fenêtre globale 'window' ⚠️
    ☞ Permet de renvoyer plusieurs valeurs / objets

``CamelCase`` = notation commence par une minuscule puis à chaque mot par une majuscule (_ex : maFonction_)    
``PascalCase`` = notation commence par une majuscule + à chaque nouveau mot     
``méthode`` = fonction d'un objet      
`new` = grâce à ce mot clé 'new', 'this' pointe vers l'objet → va créer un nouveau objet vide '{}'     
`factory function` = c'est une usine qui permet de créer des objets qui renvoie les mêmes propriétés

- JS
````javascript
const video = {
    title: "Titre";
    play(){
        console.log('1', this); // "this" va faire référene à l'objet : "video"
    }
};
video.play();

//CONSTRUCTOR FUNCTION
function Video(title){ // pascale case, récupérer 'title' + 
  this.title = title; // enregistrer dans l'objet 'this'
  
  // créer un objet grâce à l'opérateur 'new'
  console.log('2', this);
}

const video1 = new Video ("a"); // Nomme le titre // opérateur 'new' va créer un objet vide '{}' le mot cle 'this' va faire référence à cet objet là 

//FACTORY FUNCTION  
function createVideo(title){ // créer 'createVideo' qui va récupérer un titre
   console.log('3', this);
    return{                 // va renvoyer un objet vide 
        title: title, // lui donne la propriété 'title'
    };
}
const video2 = createVideo('3');
console.log('video 2 :', video2);


````



**RÉSULTAT** :           
![arrow-function](../assets/angular-arrowfuction.png)

---
 ## MODULES 

---

    Permet de regrouper des composants (services, directives, pipes) , définir leurs dépendances + définir leur visibilités

![modules](../assets/angular-modules.png)

![ngmodule](../assets/angular-ngmodule-base.png)

- TS

````ts
// ☞ NgModule de base 
//app.module.ts
import {NgModule} from '@angular/core'; // ficher dans 'node_modules library root' 
import {BrowserModule} from "@angular/platform-browser";

@NgModule({ // décorateur
  //paramètre
  declarations: [ // Component, Pipe, Dicrectives
    AppComponent, 
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
  ],
  providers:[], // Services 
  bootstrap: [AppComponent] 
})
export class AppModule {
    
}
````

**Le décorateur @NgModule** :
→ Modifie une classe de Javascript pour ajouter des méta-données, Permet à Angular de connaître le rôle de la classe + alimenter des propriétés :      
`declarations`: Propriété, déclare des classes de vue du module (component, pipe, directive)          
`exports` : Propriété, déclare le sous-ensemble de déclaration visible par les autres modules     
`imports` : Propriété, déclare `BrowserModule` dispose des services spécifiques au navigateur (DOM)       
`providers` : Propriété, endroit où est réportrier les services dont l'application a besoin     
`bootstrap` : Définit le **root** component, contient l'ensemble des autres components  


✅ À RETENIR ✅     
- `AppModule` importe `BrowserModule` comme dépendance, nécessaire pour exécuter Angular dans Browser    
- `AppModule` déclare component `AppComponent` qui sera dans le component root     
- 

---

## ROUTING

---

  Angular est de créer une "**single page application**" (SPA),    
  Au **lieu de charger une nouvelle page à chaque clic** ou à **chaque changement d'URL**, on **remplace le contenu** ou une **partie du contenu de la page** : on modifie les components qui y sont affichés, ou le contenu de ces components. On **accomplit tout cela avec le "routing"**, où **l'application lit le contenu de l'URL pour afficher le ou les components requis**.
  
![routing](../assets/angular-routing.png)

---
### CRÉER DES ROUTES
  
![routing1](../assets/angular-routing-1.png)
  
☞ Route dans une application Angular  
→ C'est des instructions d'affichage pour chaque URL, → quel component faut afficher dans quel endroit dans l'URL     
→ Déclare les routes dans `app.module.ts`    
→ Créer une constante `Routes` → importe depuis `@angular/router` + array d'objets Js ⬇️       

- TS

````ts
// fichier app.module.ts
import {AppComponent} from "./app.component";
import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {RouterModule} from "@angular/router";


const routes: Routes =  // déclare variable "routes", typé "Routes"
  // va afficher dans le URL, chaque composant séparé par "/" 
  {path: 'contact', component: ContactComponent}, // 1er Route
  {path: 'users/info', component: InfoComponent},// 2e Route
  {path: 'users', component: UsersComponent}, // 3e Route
  {path: '', component: DashboardComponent}, // 4e Route,path = chemin : ici une string vide
  
   
 
]; 


@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    DashboardComponent,
    InfoComponent,
    ContactComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes), // Appel la fonctionnalité ".forRoot", en paramètre "routes"
  ]
})
````

❗️ **À NE PAS METTRE " / " DANS PATH SINON ERROR** ❗️    
`{path: }` : chemin d'accès, correspond à une string → qui viendra après `/` dans URL    

- ENREGISTRER DANS L'APPLICATION    
  - Importe `RouterModule` depuis `@angular/router`     
  - Ajoute **array** depuis `AppModule`    
  - Appel la méthode `forRoot()`
  
☞ **Afficher les components** dans le **template** lorsque l'utilisateur navigue vers la route en question <router-outlet></router-outlet>`  :

`````html
//fichier app.component.html

<div class="container">
  <div class="my-5">
    <h1>Routing</h1>
  </div>
  
  <router-outlet></router-outlet>

  <app-navbar></app-navbar> //mettre en dessous les routes, pour ensuite faire afficher la navbar HTML
  
  
</div>
``````

✅À RETENIR✅   
→ À chaque fois quand écrit dans URL = un composant    
_Exemple_ :     
localhost:4200/users ↔  va afficher le composant "user",      
localhost:4200/contact ↔  va afficher le composant "contact",      
localhost:4200/ ↔ va afficher le composant "dashboard" ( car ici c'est une string vide )     

----
### NAVIGUEZ AVEC LES ROUTERLINK

(Utilise Bootstrap pour les components HTML)    
Pour que l'utilisateur navigue à l'intérieur de l'application  → faut **créer** des **liens** / **boutons** qui naviguent vers les routes crées précédemment     
`routerLink=" "`  : attribut / lien qui renvoie au composant souhaité en cliquant sur ce bouton 

- HTML
````html
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Navbar</a>
  
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" routerLink="/">Dasboard</a>
        </li>
        
        <li class="nav-item">
          <a class="nav-link" routerLink="/users">Users</a>
        </li>
        
        <li class="nav-item">
          <a class="nav-link" routerLink="/users/info">Info</a>
        </li>

        <li class="nav-item">
          <a class="nav-link" routerLink="/contact">Contact</a>
        </li>
       
      </ul>
    </div>
  </div>
</nav>
````

⭕️ CLASS ACTIVE

class **active** : vérifie si l'itinéraire lié d'un élément est actuellement actif + permet de spécifier une/ plusieurs classes CSS à ajouter à l'élément lorsque l'itinéraire lié est actif ; Angular fournit un attribut pour cela qui peut être ajouté au lien directement ou à élément parent              
`routerLinkActive="active"`: mot clé           
`[routerLinkActiveOption]="{exact: boolean}` : Permet d'ajouter les classes uniquement lorsque l'URL correspond exactement au lien

- HTML 
`````html
<ul class="navbar-nav">
  <li class="navbar-item" routerLink="/" routerLinkActive="active" [routerLinkActiveOptions]="{active: true}"> // Mets une condition si respect la condition  "true" va être un lien active, car si ne mets pas ça URL va prendre les classes qui contient "/" y compris lui 
    <a class="nav-link" >Dashboard</a>
  </li>
  
  <li class="navbar-item" routerLink="/users" routerLinkActive="active">
    <a class="nav-link">Users</a>
  </li>
  
  <li class="navbar-item" routerLink="/contact" routerLinkActive="active">
    <a class="nav-link">Contact</a>
  </li>
</ul>
`````

---

## ROUTING PARAMS

---

☞ Paramètre dans l'URL / des routes

`:` = paramètre, déclare ce fragment comme étant un paramètre

- TS
````ts
// fichier app.modules.ts
const routes: Routes = [
  {path: 'users/:id', component: InfoComponent}, // insére un paramètre en 'id'
  {path: 'users', component: UsersComponent},
]
````

````ts
// fichier users.component.ts

import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit{
    //paramètre de 'id'
    users: {id: number, name: string}[]=[
      {id: 1, name: 'John'},
      {id:2,  name: 'Sam'},
    ]
}
````

- **RÉSULTAT**            
![params](../assets/angular-routing-params.png)

- HTML 
`````html
// fichier users.component.html

<p>users works!</p>

<ul>
  // (loccalhost:4200/users/2) (exemple ici)
  
  //➡️SYNTAXE 1
  <li *ngFor="let user of users" [routerLink]="[user.id]"> // va rendre dynamic → quand appuie sur un des listes va rajouter 'id' concerné dans URL
   
    {{ user.id}} . {{user.name}}</li>
</ul>
`````

`[routerLink]="[]"` = va ajouter dans une autre route dans l'URL 

`````html
//➡️SYNTAXE 2
<li *ngFor="let user of users" [routerLink]="'/users/' + user.id">
  {{user.id}}. {{user.name}}
</li>
`````

---

### CONSTRUCTION DYNAMIQUE 

- HTML
`````html
// fichier info.component.html
<p>info works!</p>

<button class="btn btn-primary" reterLink="..">Retour</button>
`````

`..` = revient en arrière comme HTML/CSS    
`/` = racine du projet + ajoute le nom du fichier(route)

--- 

### CONSTRUCTION AVEC PARAMÈTRES (URL RELATIVE DANS TEMPLATE)

- TS

````ts
import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-info',
  templateUrl: './infos.component.html',
  styleUrls: ['./infos.component.scss'],
})
export class InfoComponent implements OnInit {
    
    public id: string; // quand va appuyer sur 'id' va afficher un string
    
    // quand le composant va être construit → angular va se charger d'insérer class 'ActivatedRoute' dans la propriéte 'route'
  constructor(private route: ActivatedRoute) {
  }
  
  ngOnInit(){
      console.log(this.route.snapshot.params.id);  // va afficher les paramètres + informations de URL 
      this.id = this.route.snapshot.params.id;
  }
}
````

- ☞ console 
  - va afficher tous les paramètres effectués dans `snapshot` 
  -  dans `params` toutes les informations de routes dans URL 
  
- TS
````html
<p>info works!</p>

<p>Id de l'utilisateur : {{ id }}</p> // va afficher ce paragraphe dans appuie un des 'id' 

<button class="btn btn-primary" routerLink="/users">Retour</button>
````

- **RÉSULTAT**
![params](../assets/angular-routing-params-1.png)
  
----

## ROUTER SERVICE

---
    

     Permet de déclencher la navigation via la méthode 'navigate', 'navigateByUrl', 'this.router.navigate(['/', id], {queryParams:{}})'
    → construire + analyse les URL de routing
    → Veérifie si une "route" est actuellement visitée

⭕️ CHANGER DE URL DE MANIÈRE PROGRAMMATIQUE

- HTML
````html
// fichier users.component.html
<ul>
  <il *ngFor="let user of users" 
    (click)="showInfo(user.id"> // créer un bouton pour écouter un évènement (retourner une info) (dans fichier ts)
    {{ user.id }}.{{ user.name }}
  </il>
</ul>
````

- TS

`````ts
// fichier users.component.ts
import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit {
  users: { id: number, name: string }[] = [
    {id: 1, name: 'John'},
    {id: 2, name: 'Sam'},
  ];

  constructor(private router: router) {
  }
  
  ngOnInit(){
      // Au bout de 3sec → redirige vers info de l'utilisateur 1
    
    setTimeout(() =>{
        this.router.navigateByUrl('/users/1');
        
    }, 3000); // = 3sec (1sec = 1000)
  }
  
  public showInfo(id: number){ // créer une propriété
      // va retourner automatiquement à l'utilisateur 1 au bout de 3sec + ajoute '1' dans URL 
     setTimeout(() =>{
         this.router.navigateByUrl('/users/' / id);
     }, 3000);
  }
}
`````

`{queryParams: {}}` : C'est des données qu'on insère dans l'URL 

```ts
public showInfo(id: number){
    setTimeout(() =>{
        this.router.navigate(['/users', id], {
            queryParams: {sortBy: 'name'}});
    }, 3000);
}
```
![router-service](../assets/angular-routing-router-service.png)

⭕️ URL RELATIVE PROGRAMMATIQUE 

- TS

```ts
// fichier info.component.ts
import {Componen, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-info',
  templateurl: './info.component.ts',
  styleUrls: ['/info.component.scss'],
})
export class InfoComponent implements OnInit {
  public id: string;

  constructor(private router: Router,
              private route: ActivatedRoute) { // corresponds à 'route' dans 'this'
  }
  
  ngOnInit(){
      console.log(this.route.snapshot.params.id);
      this.id = this.route.snapshot.params.id;
  }
  
  public goBack(){
      const url = '..'; // va retourne la page 
      this.router.navigate(['url'], {relativeTo: this.route})
  }
}
```

--- 
 ## COMPONENT LIFECYCLE

---
    
    Permet d'accéder aux évènements clés du cycle de vie d'un composant / directive afin d'initialiser de nouvelles instances
    Lance la détection des modifications si nécessaire
    Réponds aux mise à jour lors de la détection des modifications + mettoie avant la supression des instances
  
Une **instance de composant a un cycle de vie** qui commence lorsque Angular **instancie la classe de composant** + **restitue** la vue du composant avec **'enfants'**. 
LifeCycle se poursuit avec la détention des **modifications**, car Angular **vérifie** si les **propriétés liées aux données changent** + met à jour la vue 'enfants' et l'instance de composant.   
LifeCycle se termine lorqu'Angular **détruit l'instance de composant** + **supprime** son modèle rendu du DOM. 
Les directives ont un cycle de vie similaire, car Angular crée, met à jour + **détruit des instances au cours de l'exécution**. 


![lifecycle](../assets/angular-component-lifecycle.png)



☞TOUJOURS IMPLÉMENTER avant la méthode (OBLIGATOIRE):     
`export class` lifeCycleComponent `implements` `OnChanges`, `OnInit`, `DoCheck`, `AfterContentInit`, `AfterViewInit`, `AfterViewChecked`, `OnDestroy`

1)  **ngOnChanges** :     
Première méthode qui va être appliquée, même avant l'initialisation → permet d'écouter les modifications des propriétés comme entrée des composants, tous les **input** des composants peut écouter les modifications    
☞ Lorsqu'Angular modifie une des propriétés du **component** suite à un **@Input** (_Property Binding_) ➡️ La méthode reçoit un objet **SimpleChanges** contenant les valeurs courantes + précédentes ️    
→ Avant **ngOnInit** + chaque fois une / plusieurs propriétés précédées de **@Input**    
  

```ts
// fichier lifecycle.component.ts
export class LifecycleComponent implements Onchanges {

  @Input() title = '';

  constructor() {
  }
}

//cette méthode permet d'écouter les modifications des propriétés défnies comme entrée des composants
ngOnChanges(changes
:
SimpleChanges
):
void {
  console.log('On Changes - Lifecycle :', changes);
}
```

2) **ngOnInit** :     
Méthode qui va être à l'initialisation du composant → donc va récupérer les entrées 
   (Va demander les données de l'utilisateur)     
☞ Initialise le **component** après qu'Angular ait valorisé **@Input**      
→ Une fois après le premier **ngOnChanges**
`````ts
export class LifeCycleComponent implements OnInit {

  @Input() title = '';

  constructor() {
  }
  
  ngOnInit(): void{
      console.log('On Init - Lifecycle');
  }
}
`````

3) **ngDoCheck** :    
Faire des vérifications     
☞ Détecte + agit sur les changements qu'Angular ne peut / pourra pas détecter lui même, Permet de vérifier les changements dans les directives additionnellement aux algorithmes classiques    
→ Immédiatement après **ngOnChanges** + **ngOnInit**

```ts
//fichier lifecycle.component.ts

export class LifeCycleComponent implements DoCheck {
  
    @Input() title = '';

  constructor() {
  }
  
  ngDoCheck(): void{
      console.log('Do Check - Lifecycle');
  }
}
```

4) **ngAfterContentInit** :     
☞ Se lance après l'initialisation d'un Content (**ngContent**). **@ContentChild** + **@ContentChildren** = valorisées      
→ Une fois après le premier **ngDoCheck**

````ts
// fichier lifecycle.component.ts

export class LifeCycleComponent implements AfterContentInit {

  @Input() title = '';

  constructor() {
  }
  
  ngAfterContentInit(): void{
      console.log('After Content Init - Lifecycle');
  }
  
}
````

5) **ngAfterContentChecked** :    
☞ Vérifie le contenu projeté dans la directive / composant     
→ Après **ngAfterContentInit** + tous les suivants **ngDoCheck**

````ts
// fichier lifecycle.component.ts

export class LifeCycleComponent implements AfterContentChecked {

  @Input() title = '';

  constructor() {
  }
  
  ngAfterContentChecked(): void{
      console.log('After Content Checked - Lifecycle');
  }
}
````

6) **ngAfterViewInit**:      
☞ Se lance après initialisation de la vue 'enfants' + component, **@ViewChild** + **@ViewChildren** = valorisées      
→ Une fois après le premier **ngAfterContentChecked**

````ts
//fichier lifecycle.component.ts

export class LifeCycleComponent implements AfterViewInit {

  @Input() title = '';

  constructor() {
  }
  
  ngAfterViewinit() : void{
      console.log('After View Init - Lifecycle');
  }
}
````

7) **ngAfterViewChecked** :      
☞ Vérifie les vues component + enfants     
→ Après **ngAfterViewInit** + **ngAfterContentChecked**

````ts
// fichier lifecycle.component.ts

export class LifeCycleComponent implements AfterViewChecked {

  @Input() title = '';

  constructor() {
  }
  
  ngAfterViewChecked () : void{
      console.log('After View Checked - Lifecycle');
  }
}
````

8) **ngOnDestroy** :     
Nettoyer le composant     
☞ Nettoyage juste avant qu'Angular ne détruit le composant     
→ Avant la destruction du component / directives

````ts
// fichier lifecycle.component.ts

export class LifeCycleComponent implements OneDestroy {

  @Input() title = '';

  constructor() {
  }
  
  ngOnDestroy() : void{
      console.log('On Destroy - Lifecycle');
  }
}
````

----

