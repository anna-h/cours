 

![rxjs-logo](../assets/angular-rxjs-logo.png)

---
## PROGRAMMATION RÉACTIVE AVEC LA LIBRAIRIE RxJs

---

- Programmation réactive = "réagir au évènement (fonction / logique)", se base sur le concept d'**observateur**. Le principe est de définir des **observables** et des **observateurs**. Les **observables** vont émettre des évènements qui seront interceptés par les **observateurs**      
→ La _programmation réactive_ va étendre ce concept en permettant de combiner les **observables**, **modifier** les évènements à la volée, les filtres etc    
  
   
    Pour réagir à des évènements / données de manière asynchrone ( pas devoir attendre une tâche ( ex: appel HTTP, soit terminer avant de passer à la ligne du code)) 
➡️ Réagir au élément (ex: DOM)   
➡️ Peut être des demandes de données au serveur ( requête : HTTP ...), quand je ne sais pas quand tu va avoir une réponse, mais lorsqu'on a une réponse, veut réagir → à une programmation réactive

![rxjs](../assets/angular-rxjs.png)


- Observable : flux de donnée / est un objet qui émet des informations auxquelles on souhaite réagir. → peuvent venir d'un champ de texte dans lequel l'utilisateur rentre des données / progression d'un changement de fichier , Objet de base de la programmation réactive
- Observer : est un bloc de code qui sera exécuté à chaque fois que l'Observable émet une information → 3 fonctions callback (méthode)

**Récupérer la librairie RxJs**
https://github.com/ReactiveX/rxjs 

![installation](../assets/angular-rxjs-install.png)

- JS
```javascript
//fichier index.js

//SYNATXE 1
new rxjs.Observable

//SYNTAXE 2
const {Observable} = rxjs;

//⭕️ créer un flux
const flux = new Observable(function (observer){ //fonction arrow
    observer.next(1);//va envoyer une donnée "1"
    observer.next(10);
    observer.next(100);
    
    setTimeout(() =>{ //fonction fléchée
        observer.next(20);//envoie une nouvelle valeur
        observer.complete();//va compléter le observeur 
    },2000)
});

//⭕️ créer un observer
const abonnement = flux.subscribe(
    //fonction callback
    function (value){
        console.log('valeur :', value);
    },
    function (err){
        console.log('Erreur :', err);
    },
    function (complete){
        console.log('Complète');
    },
);


setTimeout(() =>{ //mets le settimetout ici car il est désabonné ⬆️, donc il faut le remttre pour qu'il réaparaise même arpès le désabonnement
    abonnement.unsubscribe();//si on se désabonne pas la flux va continuer à tourner
}, 3000);
```

![resultat](../assets/angular-rxjs-1.png)

Écouter un flux de donnée (bouton)
- JS
```javascript
//fichier index.js
const {Observable, fromEvent} = rxjs;
const {map} = rxjs.operators;

const button = document.querySelector('button');

let count = 0;
const subscription = fromEvent(button, 'click')
    .pipe(     //faire des modifications
        map((value) =>{
            console.log('value', value); //va afficher l'évènement de l'ecoute
            return 50;
        }),
    ) 
    .subscribe(() =>{
        console.log(++count, value);
    })

subscription.unsubscribe();

```