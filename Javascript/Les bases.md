
![js](../assets/JS.png)

 

    Js est un language de programmation de script employé pour avoir des pages internet interactif.
    C'est-à-dire de dynamiser et animer les pages internet.


````console.log();```` = Pour vérifier son code JS, dans la console 


## SOMMAIRE 

---


-  [Variable](#variable-)
-  [Type de variables](#-type-de-variables-)
- [Opérateurs](#-oprateurs)
  - [Opérateurs arithmétique](#oprateur-arithmtiques-)   
  - [Opérateurs de comparaison](#-oprateur-de-comparaison-)   
  - [Opérateurs logiques](#-oprateur-logiques-)      
  - [Opérateur d'affectation](#oprateur-daffectation-)
-  [Strings](#strings)    
   - [Syntaxe](#syntaxe-)     
   - [Accéder à un caractère grâce à sa position](#accder--un-caractre-grce--sa-position)        
   - [La méthode "IndexOf"](#la-mthode-indexof)  
   - [Concaténation](#concatnation)
-  [Les Condition](#-les-conditions)    
   - [Les conditions if/else](#les-conditions-ifelse)       
   - [Le Ternaire](#le-ternaire)       
   - [Switch](#switch) 
-  [Les boucles](#les-boucles)
    - [La boucle for](#la-bouche-for)
   - [L'instruction while](#instruction-while)     
   - [L'instruction do...while](#linstruction-dowhile)     
- [Les Fonctions](#les-fonctions)   
     - [Déclarer une fonction](#dclarer-une-fonction--linstruction-function)      
     - [Fonction anonyme](#fonction-anonyme)      
     - [Fonction callback](#fonction-callback)
     - [Fonction fléchée](#fonction-flche-function-arrow)
- . [Les objets](#les-objets)    
     - [Déclarer un objet](#dclarer-un-objet)   
     - [Construction d'un objet](#construction-dun-objet)
- [Spread operator](#spread-operator)        
- [Affecter par décomposition](#affecter-par-decomposition) 




---
##  ⚜️VARIABLE ⚜️ 

------

    Une variabe est un contenant utilisé pour enregistrer une donnée spécifique dont le programme a besoin.
---

 **3 COMPOSANTS** = (décrivent une variable)




1.   **NOM** = on lui attribue une étiquette, cela doit refletter ce qu'il y a dans le conteneur       

 -  Doit contenir que des lettres, pas de chiffres de caractères spéciaux ou d'espace    
 - Colle les mots ensemble en utilisant une majuscule pour chaque mot sauf le premier. 


_Exemple_ : maDateDeNaissance



2.  **VALEUR** = on lui donne une valeur    
 - Pour déclarer une variable, on utilise le mot clé __let__ suivi du nom de la variable puis __=__ et de la __valeur__ 


_Exemple_ : 

```js
let age = 21
``` 

Lorsqu'on assigne une valeur à une variable, c'est-à-dire stock valeur dans une variable pas encore stocker = **Initialiser**    
≠ si on utilise une variable sans initialiser = l'appli ne fonctionnera pas + peut être planté


 3. **TYPE** = va préciser ce qu'il y a dedans (en nombres / du texte)     

---

**3 mots clés** : (déclare une variable)

`let` = déclare variables qui peuvent changer durant l'exécution du programme    
`var` = ancien syntaxe, aussi déclare variable + initialiser sa valeur 

≠ pas obliger de déclarer variable avant de manipuler le code = **remontée/hoisting**


````js
//Fonctionne
prenom = 'Anna';
var prenom;

var prenom = 'Anna';
var prenom = 'Tiphanie';

//Fonctionne pas + Erreur 

nom = 'Anna';
let nom;

let nom = 'Anthony';
let nom = 'Tony';
````


`const` = déclare variables qui ne change pas 



## ⚜️ TYPE DE VARIABLES ⚜️

     va classifier les différentes variables = catégorie

 ````typeof```` = mot clé

- **Number** = nombres

````js
let age = 21;
````

- **Boolean** = booléens

````js
let vrai = true; 
let faux = false; 
````

_Exemple :_ 

_8 strictement supérieur à 4 = résultat retourne à "**true**"_    
_8 strictement inférieure à 9 = résultat retourne à "**false**"_

 
- **String** = chaîne de caractères


````js
let name = 'Anna';
````
---

## ⚜️ OPÉRATEURS

---

### ⚜️OPÉRATEUR ARITHMÉTIQUES ⚜️

---

- ⭕️ ADDITION ( **+** ,somme)
````javascript
let add1 = 1;
let add2 = 2;

let somme = add1 + add2;

console.log('Addition:' add1, add2, somme);
````

- ⭕️ SOUSTRACTION ( **-** ,diff)
`````javascript
let subs1 = 1;
let subs2= 2; 

let diff = subs1 - subs2;

console.log('Soustraction :', subs1, subs2, diff);
`````

- ⭕️ MULTIPLICATION ( __*__ ,produit)
````javascript
let multi1 = 1; 
let multi2 = 2;

let produit = multi1 * multi2;

console.log('Multiplication :', multi1, multi2, produit);
````

- ⭕️ DIVISION ( **/** ,quotient)
````javascript
let div1 = 1;
let div2= 2;

let quotient = div1 / div2;

console.log('Division :', div1, div2, quotient);
````

- ⭕️ PUISSANCE ( __**__ )
````javascript
let puiss1 = 1;
let puiss2 = 2;

let result = puiss1 ** puiss2; // 2²

console.log('Puissance :', puiss1, puiss2, result);
````

- ⭕️️ INCRÉMENTATION ( **++** )   
→(inc **++**) : quand mets l'incrémentation après, va renvoyer la valeur avant l'incrémentation    
→(**++** inc) : quand mets l'incrémentation avant, va renvoyer la valeur après
````javascript
let inc = 0;

inc++; 

console.log('Incrémentation 1 :', inc++);
console.log('Incrémentation 2 :', ++inc);
````

- ⭕️ DÉCRÉMENTATION ( **--** )   
→ (dec **--**) : quand mets la décrémentation après, va renvoyer la valeur avant        
→ (**--** dec) : quand mets la décrémentation avant, va renvoyer la valeur après

```javascript
let dec = 0;

console.log('Décrémentation 1:');
console.log('Décrémentation 2:');
```

- ⭕️ PLUS UNAIRE ( **+** string)
````javascript
let unaire = "12";

console.log('Plus unaire: :', +unaire);
````
---

### ⚜️ OPÉRATEUR DE COMPARAISON ⚜️

--- 

    Permet de comparé 2 éléments entre eux + 'return' des booléens

- ⭕️ ÉGALITÉ SIMPLE ( **==** )
```javascript
let nb1= 10; 
let nb2= 12; // si nb2 est un string (nb2 = "10"), return 'true' → car analyse la valeur non 'type'

console.log('Egalité simple: ', nb1 == nb2); // Demande si nb1 'égale' à nb2 + 'return' false
```

- ⭕️ INÉGALITÉ SIMPLE ( **!=** ) "soit différent à égale"
````javascript
let nb3 = 30;
let nb4 = "19";

console.log('Inégalité simple :', nb3 != nb4); // return 'true'
````

- ⭕️ ÉGALITÉ STRICT ( **===** ) "strictement égale"
````javascript
let nb5 = 10;
let nb6 = 10;

console.log('Egalité strict :', nb5 === nb6);
````

- ⭕️ INÉGALITÉ STRICT ( **!==** )
````javascript
let nb7 = 30;
let nb8 = 30;

console.log('Inégalité :', nb7 !== nb8); // return 'false', car ils sont égaux 
````

- ⭕️ STRICTEMENT SUPÉRIEUR ( **>** )
````javascript
let nb9 = 9;
let nb10 = 10;

console.log('Sctrictement supérieur :', nb9 > nb10)
````

- ⭕️ STRICTEMENT INFÉRIEUR (**<**)
````javascript
let nb11 = 10 ;
let nb12 = 11;

console.log('Strictement inférieur :', nb11 < nb12);
````

---

### ⚜️ OPÉRATEUR LOGIQUES ⚜️

---

- **ET** LOGIQUE ( expression1 **&&** expression2 )
````javascript
let nb1 = 2;
let nb2 = 2; 
let disabled = false;
let loading = true;
let name = 'Anna';

console.log('Et Logique :', nb1 === nb2); // nb1(2) === nb2(2) : true (expression1) // name('Anna') === "Anna" : true (expression2) // = va afficher TRUE
````

- **OU** LOGIQUE ( expression1 **||** expression2)
````javascript
let price = 499;
let adult = true;
let city = "Paris";

console.log('Ou Logique :', price > 300 || adult || city === 'Paris'); // tous les élements corresponds au condition = TRUE 
````

- **NON** LOGIQUE ( **!** expression, **!!** : l'inverse de l'inverse ) **l'inverse**    
→ ex: !true = false  
→ **!!** = Permet de savoir 3 éléments d'une seule variable ( return : boolean)
````javascript
let nullVar = null;
let undefinedVar; // ＝ undefined
let prod = false;
let count = 0;
let str = "";

console.log('Non Logique :', !nullVar, !undefinedVar, !prod, !count, !str ); // = TRUE
````
**!str (✅SIMPLIFICATION✅) → str !=null && str == undefined && str != ""**


---

### ⚜️OPÉRATEUR D'AFFECTATION ⚜️

---

    Assigne le valeur à une variable 

- AFFECTATION SIMPLE ( **=** )
```javascript
nb = 89;
```

- AFFECTATION APRÈS ADDITION ( **+=** )
```javascript
nb += 3; // nb = nb + 3
```

- AFFECTATION APRÈS SOUSTRACTION ( **-=** )
````javascript
nb -= 5; // nb = nb - 5
````

- AFFECTATION APRÈS MULTIPLICATION ( ***=** )
````javascript
nb *= nb2; // nb = nb * 2
````

- AFFECTATION APRÈS DIVISION ( **/=** )
````javascript
nb /= 3; // nb = nb / 3
```` 

- AFFECTATION DU RESTE ( **%=** ) **%= modulo : permet de retourner une division** 
```javascript
nb %= 4, // nb = % 4
```




---

## ⚜️STRINGS

----
 ````str```` : abréviation de string

---
### ⚜️SYNTAXE ⚜️

---
````js
let str = "nom1";
let str = 'nom2';
let str = `nom3`;
````

 ⚠️ A️ utiliser une seule manière tout au long du projet ⚠️

- **Échapper les caractères** = pour échapper à tous les guillemets, citations etc.. = ( \ ) à mettre au débout + la fin des guillemets concernés

````js
str = 'C\'est';
str = "C'est\"Mardi\"";
str = "Nom avec /!\\Attention/!\\";

console.log('echappement:',str);
`````

➡️ La console est faite pour voir son résultat de son code js ≠ _la console prendra dernière str_

- **Saut de ligne** = ( \n ) , peut mettre au tant qu'on veut à la suite </br>

````js
str = "Mon texte \n avec des fleurs";
````
↑︎ Pour voir le résultat ``console.log(str);``

     \n\n\n = saute 3 lignes
     \n\r = saute 1 ligne + 1 espace

`` \r = espace dans une phrase / string``

- **Mettre en majuscule**

````js
console.log(str.toUpperCase(),);
````

- **Mettre en minuscule**

````js
console.log(str.toLowerCase(),); 
````

- **Longueur d'une string** = savoir combien de caractères à dans une string 

````js
console.log(str.length);
````
---

### ⚜️ACCÉDER À UN CARACTÈRE GRÂCE À SA POSITION⚜️

---

`[]`: mettre un nombre dans l'accolade 

❗️LA 1ᵉ POSITION EN JS COMMENCE PAR 0❗️

````javascript
const str = "Anna"; // la première lettre position 0

console.log(str[0]);
````
---

### ⚜️LA MÉTHODE "IndexOf()"⚜️

    Méthode qui renvoie la position d'une chaîne partielle à partir d'une position déterminée
    Elle renvoie -1 si la valeur cherchée n'est pas trouvée
`indexOf` : mot clé
```javascript
const str = "Bonjour, Anna";
const search = "Jean";

console.log(str, searchstr.indexOf(search)); // va être à la 9ᵉ position dans la console 
```

---

### ⚜️CONCATÉNATION⚜️

-----

    Rassembler deux chaines de caractères pour en former une  

````js
let one = 'Sam';
let two = 'Clover';

console.log(firstName + lastName);

Resultat = SamClover
````

≠ 

````js
let firstName = 'Sam';
let lastNAme = 'Clover';

console.log(firstName - lastNAme);

//Resultat = NaN (Not A Number) = Error 
````


❌ (-) Ne peut être utilisé que par des nombres ❌


- **Avoir un espace**


`````js
let firstName = 'Tony';
let lastName = 'Jeff';

console.log( firstName + '' + lastName); 

//va afficher Tony Jeff
`````

---
## ⚜️ LES CONDITIONS 

---

### ⚜️LES CONDITIONS "IF/ELSE"⚜️

---

 `if` = si   
 `else` = sinon   
 `else if` = sinon si 

- si/sinon = condition remplie 

  -  Premier bloc de code exécuté
 
- sinon

  -   Deuxième bloc de code exécuté



````js
let age = 17;

if (age >= 18 ){ // SI "age supérieur ou égale à 18
    console.log("Tu es majeur! "); // va afficher "Tu es majeur !"
    
}else if (age >= 13 && age < 18){ //SINON SI "age entre 13 et 18"
    console.log("Tu es ado ! "); // va afficher "TU es ado !"
    
}else { // SINON
    console.log("Tu es mineur ! "); //va afficher "Tu es mineur !"
}
````

- **Avec boolean**

````js
let userLoggedIn = false;

if (userLoggedIn){ //SI
    console.log('Utilisateur connecté'); // va afiicher "Utilisateur connecté"
}else { // SINON
    console.log('Utilisateur non connecté'); // va afficher "Utilisateur non connecté"
}
````
---

### ⚜️LE TERNAIRE⚜️

----

    Permet d'écrire des conditions plus condensées + alléger nos scripts + gagner du temps à développer
    Va utiliser l'opérateur de comparaison 
`(condition) ? expreSiVrai : expreSiFaux` : Syntaxe

_Exemple_ :    
**SI** la variable "presence" est TRUE    
**ALORS** J'affiche "Bonjour" dans la console    
**SINON** J'affiche "Bye"

````javascript
let presence = true;

if (presence === true){
    console.log("Bonjour");
} else {
    console.log("Bye");
}
````

➡️ **TERNAIRE** (✅SIMPLIFICATION✅)
````javascript
console.log((presence === true) ? "Bonjour" : "Bye");
````
----

### ⚜️SWITCH⚜️

---

    Permet de réaliser des conditions en fonction de l'expression
`switch(expression){ case valeur1: [break] }` : mot clé, utilise ce cas si on connait une valeur déjà définie dans une correspondance → c'est préférable d'utiliser **switch** que **else if** (else if utilise quand c'est plus complexe)

**SYNTAXE**       
![switch](../assets/switch-js.png)
````javascript
//EXEMPLE

let fruits = 'Oranges';

if (fruits === 'Pommes'){
    console.log('Les pommes valent 1€ le kilo');
}else if ( fruits === 'Fraises'){
    console.log('Les fraises valent 2€ le kilo');
}else if ( fruits === 'Bananes' || fruits === 'Cerises'){
    console.log('Les bananes et les cerises valent 3€ le kilo');
}else {
    console.log('Vous n\'avez aucun fruits');
}

switch (fruits){
    case "Pommes":
        console.log('Les pommes valent 1€ le kilo');
        break; // METTRE UN BREAK POUR CASSER LE CAS/ VALEUR SINON ÇA VA CONTINUER
    case "Fraises":
        console.log('Les fraises valent 2€ le kilo');
    case "Bananes": 
    case "Cerises":
        console.log('Les bananes et les cerises valent 3€ le kilo');
        break;
    default:
        console.log('Vous n\'avez aucun fruits');
        break;
}
// VA AFFICHER LA VALEUR PAR DÉFAUT, CAR N'A PAS DÉTERMINER CASE "ORANGE"
````




## ⚜️LES BOUCLES 

---

### ⚜️LA BOUCHE "FOR"⚜️

---

    Permet de répéter des actions 

```js
for ([exprssionInitiale];[condition];[expressionIncrément/Décrémenter]){
    instructions
}
```

-  La condition doit être TRUE pour que ça soit affiché     
-  Toujours définir la condition sinon la condition va être à l'infini et ça va faire planté le navigateur

️ **⭕️INCRÉMENTER** 

````js
for (let i = 0; i < 10; i++){
    console.log(i);
}

//Résultat : console va afficher 0 jusqu'à 9
````


 **Condition** = **TRUE** : la boucle exécutée
≠ **Condition** = **FALSE** : la boucle ``for`` se termine
 

**⭕️DÉCRÉMENTER** 

````js
for (let i = 10; i >= 0; i--){
    console.log(i)
}

//Résultat = console affiche 10 à 0
````

➡️ Peut aussi créer un tableau dans une boucle 

````js
let arr = [2,3,4,5];
console.log(arr);

for (let i = 0; i < arr.length; i++){
    console.log(i,arr[i]);
    // on peut aussi multiplier 
   console.log(i,arr[i] *2);
}
console.log("Fin de la boucle");
````
---
### ⚜️INSTRUCTION "WHILE"⚜️

---

    Permet de créer une boucle qui s'exécute tant qu'une condition est vrai

```js
while (condition){
    instuction
}
```

---
### ⚜️L'INSTRUCTION "DO...WHILE"⚜️

---

    Permet de répéter un ensemble d'instructions jusqu'à ce qu'une condition donnée ne soit plus vérifiée

 ``faire ...tant que`` = traduction en français    
 ``tant que`` ma condition dans **while est vrai** tout ce qui se trouve apres dans **``do`` sera répété**


````js
do {
   instruction
} while (condition)
````


````js
let i = 0;
do {
   i++; // i = i + 1 ( a chaque tour i prend +1)
   console.log(i);
}while (i < 10);
````
---
## ⚜️LES FONCTIONS 

---

```function``` = mot clé

➡️ Décrit une action en détail : étape par étape    
➡️ Bloc de code nommé + réutilisable + but d'effectuer une tâche précise

````js
function carré(nombre) {
   return nombre * nombre;
}

// carré = nom de la fonction
// nombre = argument 
// retrun = spéficie valeur renvoyée par la fonction 
````
---
### ⚜️DÉCLARER UNE FONCTION ( l'instruction function)⚜️

---


````js
function nom([param, [ param, [  ... param]]]){
    instructions
}

//nom = nom de la fonction
//param = argument de la fonction 
// instructions = corps de ma fonction
````


<br>

*Exemple :*

````js
if (firstUser.online){
    if (firstUser.accountType === "normal"){
        console.log("Hello" + firstUser.name + "!");
    } else {
        console.log("Welcome back premium user" + firstUser + "!");
    }
}

if (secondUser.online){
    if (secondUser.accountType === "normal"){
        console.log("Hello" + secondUser.name + "!");
    } else {
        console.log("Welcome back premium user" + secondUser.name + "!");
    }
}

if (thirdUser.online){
    if (thirdUser.accountType === "normal"){
        console.log("Hello" + thirdUser.name + "!");
    } else {
        console.log("Welcome back premium user" + thirdUser.name + "!");
    }
}
````

Il y a trop de répétition : donc y a un raccourcir de se code 

➡️ si y a + de 2 lignes de codes = utilise les fonctions

``` → Factoriser le code dans une fonction```


````js
const sendWelcomeMessageToUser = (user) => {
    if (user.online){
        if (user.accountType === "normal"){
            console.log("Hello" + user.name + "!");
        } else{
            console.log("Welcome back premium user" + user.name + "!");
        }
    }
}

sendWelcomeMessageToUser(firstUser);
sendWelcomeMessageToUser(secondUser);
sendWelcomeMessageToUser(thridUser);
````
----
### ⚜️FONCTION ANONYME⚜️

---

    Fonction qui n'a pas de nom
    Fonction pas de nom dans un endroit dans le script + n'est pas réutilisé 

````js
var maFonction = function (){
    /* instructions */
}
````
----

### ⚜️FONCTION "CALLBACK"⚜️

----

     "fonction de rappel" = traduction en français
    ➡️ C'est une fonction qui est passée dans une autre fonction en tant qu'argument, qui à ensuite invoquée à l'intérieur de la fonction une action / routine
---
### ⚜️FONCTION FLÉCHÉE (FUNCTION ARROW)⚜️

---

    Les fonctions fléches sont souvent anonymes + pas déstinées à être utilisées pour déclarer des méthodes 
    Permet d'avoir une syntaxe plus coutre que les expressions de fonctions + ≠ possède pas de ses propres valeurs pour :
``this``, ``arguments``, ``super``, ``new.target``    
``this`` = référence l'objet dans laquelle qui est exécutée par la fonction  
Il fait référence à la fenêtre  
``=>`` = va lier  l'objet parent à l'objet enfant



````javascript
// Déclare 1er fonction
function maFonction(nb){
    console.log(this); 
    // "this" fait référence dans l'objet dans laquelle la fonction est exécutée
    return nb;
}
// Déclare fonction fléchée
let maFn = (nb) => {
    console.log(this);
    return nb;
};
// Niveau de la fenêtre (window)
maFonction(1);
maFn(2);
````

**RÉSULTAT** : (ce qu’affiche la console)       
![window](../assets/function-arrow-window.png)

- **⭕️ POUR OBJET**

````javascript
let user = {
    name: "Jean";
    notes: [12, 14, 15];
    
    getNotes: function (){ // est une méthode d'une fonction qui ne prends qu'aucun argument
        //CE "THIS" = PARENT 
        console.log(this); // "this" va faire référence à l'objet "user"
        // méthode peut accéder à d'autre propriété =>
        this.notes.forEach(function (note: number) => { // +  manipuler à "notes" fonction "callback" en argument
            
            //chaque fonction à accès à "this"
            //CE "THIS" ENFANT
            console.log(this);// ✔️ 
            console.log(note + "a eu" + note); //pour afficher "name" dans chaque fenêtre il faut faire une fonction fléchée, pour récupérer l'objet parent
      
       // ❗️METTRE EN 2E ARGUMENT❗️
        }, {name: "Anna"}) //donc va prendre le 2e "this" (enfant) 
        // va afficher le nom "Anna" au lieu de "Jean"
        
    }
};

user.getNotes();// exécute la méthode "getNotes" à travers l'objet "user"
````

**RÉSULTAT** : (ce qu'affiche la console, sans le ``this`` )     
![window](../assets/function-arrow-window-1.png)

**RÉSULTAT** : (ce qu'affiche la console, avec ``this`` +  fonction fléchée)       
![window](../assets/function-arrow-window-3.png)




---
## ⚜️LES OBJETS

---

### ⚜️DÉCLARER UN OBJET⚜️

----
```Objet``` = une représentation de donnée sous forme de clé valeur    
`````Propriété````` = chaque caractéristique lier à un objet   
``````Méthode``````= chaque action lier à un objet 



````js
//{key: value } = objet = sous clé(=propiété) : valeur
let phone = { 
    //PROPRIÉTÉS
    name:"Samsung", 
    marque: "Android",
    largeur: 5,
    longueur: 5,
    
    //MÉTHODES
    call: function (){
        console.log("Call Bibi");
    },
    sendMsg: function (to){
        console.log("Send sms to" + to);
    }
};

console.log(phone);

//POUR ACCÉDER À UNE PROPRIÉTÉ

console.log("Marque :", phone.marque); // (.) + nom de la propriété

//INVOQUER

phone.call(); // objet + (.) + méthode
phone.sendMsg("Anna"); // objet + (.) + méthode + argument ("Anna") = ce qu'on rajoute

//METHODE 1
console.log(phone.largeur);

//MÉTHODE 2
let nomPropriété = "long";
console.log(phone[nomPropriété], phone["marque"]); // clé + [""] 
//peut aussi afficher une variable : nom de la variable + entre crochet [""]
````

----
### ⚜️CONSTRUCTION D'UN OBJET⚜️

---

````this```` = À chaque objet, ce mot clé va passer en argument les valeurs relatives à ses propriétés   
 ️ ➡️ Ce mot clé sert à représenter l'objet couramment utilisé. À chaque nouvel objet créer, il va remplacer l'objet + va permettre d'initialiser différemment chaque propriété pour chaque objet


````js
//Utilisateur() = fonction constructeur

function Utilisateur(n,a,m){
    this.nom = n;
    this.age = a;
    this.mail = m;
    
    this.bonjour = function () {
        console.log("Bonjour, je suis " + this.nom[0] + "j'ai" + this.age + "ans");
    }
}

//Créer objet = "Anna" + fonction constructeur

let anna = new Utilisateur(["Anna"],21 , "tiphanie.anna@gmail.com");

//"new" = à créer une nouvelle instance

````
---
## ⚜️SPREAD OPERATOR⚜️

---

 - ```Permet d'étendre une expression de tableau / chaîne de caractères```     
- Spread = ```...``` = étaler 

**⭕️POUR TABLEAU**

````js
let arr1 = ["Anthony", "Tony", "Fabien"];
let arr2 = ["Anna", "Tiphanie", "Patricia"];

let arr3 = [];

for(let i = 0; i <arr1.length;i++){
    arr3.push(arr2[i]);
}

console.log(arr3);

//Créer un 3eme tablaeau pour mettre à la suite les 2 tableaux (en créant des boucles)
````

✅SIMPLIFICATION✅

````js
let arr3 = [...arr1, ...arr2];

console.log(arr3)
````

**⭕️POUR OBJET**

````js
let studentDefalut = {
    name: Anna,
    class: null,
};
let student = {
    ...studentDefalut,
};
console.log(student.name);


````

❌ Ne jamais recopier la même référence ≠ mais faut changer la valeur ️❌

````js
let arr1 = [1, 3];

let arr2 = [...arr1]; // Référence va pointer la donnée

arr2.push(5);

console.log(arr1, arr2);

// Le 2e tableau ne va pas être affecté , car utilise spread operator
// La valeur 5 va ête dans le tableau 2 et non tableau 1
````

----

## ⚜️AFFECTER PAR DECOMPOSITION⚜️

---

     Permet d'extraire des données d'un tableau / objet 

**⭕️Pour tableau**

````js
let a, b, rest;      

//rest va récupérer/étaler toutes les valeurs devant lui avec [...]

let arr = [1, "Anna", true, "Tony"];

[a, b, ...rest] = arr;

// a, b = va être assignée par les valeurs données = décomposition

console.log(a);
console.log(b);
console.log(rest);
````

**⭕️Pour objet**

````js
let name, age;
let student = {name: "Anna", age:21};


console.log("name:" , name, age);
````
  ____
