## SOMMAIRE
- [Les classes](#-les-classes)
    - [Déclarer une classe](#dclarer-une-classe)
    - [Propriété par défaut](#proprit-par-dfaut)
    - [La fonction "constructor()"](#la-fonction-constructor)
    - [Ajouter des méthodes](#ajouter-des-mthodes)
    - [Héritage entre les classes](#hritage-entre-les-classes)
    - [Propriétés et méthodes "static"](#proprits-et-mthodes-static)





## ⚜️ LES CLASSES

    Type de fonction qui permet de créer des objets
    Les classes sont des modèles d'objet
````class```` : Mot-clé, Nomme le nom de la classe par une majuscule + ensuite pour les autres noms
``````js
class Rectangle{
    constructor(hauteur, largueur){
        this.hauteur = hauteur;
        this.largueur = largueur;
    }
}
``````

____
### ⚜️DÉCLARER UNE CLASSE⚜️

`````js
class User {   // Modèle pour les objets
    
}
//Grâce à cette modèle là = peut créer des objets ci-dessous

let user = new User(); //= une instance
//Créer un objet : user
//New = créer un nouveau objet 

let user1 = new User();

console.log(user);
`````
____
### ⚜️PROPRIÉTÉ PAR DÉFAUT⚜️

    Ajouter par défaut des propriétés à nos classes 

`````js
class User{
    //Créer des propriétés avec des valeurs par défaut
    name = "Anna";
    age = 21;
    presence; //valeur : undefined = car valeur n'est pas défini
}

let user = new User();//Nouvelle instance = objet ; stocker dans la variable "user"


//PEUT MODIFIER LES VALEURS 

user.name = "Tony"; // Donc va afficher "Tony" à la place de "Anna" (pas oublier d'écrire sur la console pour être afficher) 


console.log(user, user.name);
// va afficher objet qui est instancié à partir de la classe "User"
`````

- Chaque nouvelle objet va avoir les même propriétés + valeurs par défaut, qui est déclaré par la classe précédemment

---

### ⚜️LA FONCTION "constructor()"⚜️

    Cette méthode est utilisé pour créer + initialiser un objet quand utulise mot clé "class"


````js
class User{
    name = "Anna";
    age = 21;
    presence;
    
    constructor(name, age, presence = false){// Déterminer arguments pour créer un nouveau objet 
        // valeur : pas besoin de modifier (mettre booléen = FALSE) 
        
        // (= name;) = ça référence argument de constuctor , pour qu'il séléctionne les valeurs modifier
        this.name= name; 
        this.age= age;
        this.presence= presence;
        // ce mot clé "this" fait référence à l'objet qui est contruit (new User)
        
    }
}

//MODIFIER LES VALEURS DANS PROPRIÉTÉS PAR DÉFAUT
let user = new User("Anthony", 12, true);//Anthony 12
let user1 = new User("Patricia", 22);// Patricia 22

console.log(user);
console.log(user1);
````

-  constructor = a des propriétés dynamiques, qui change en fonction lors de la création construction de l'objet

----

### ⚜️AJOUTER DES MÉTHODES⚜️

- Même principe que les objets


````js
class User{
    name;
    age;
    presence;
    pays = "France";
    
    constructor(name, age, presence = false){
        this.name = name;
        this.age = age;
        this.presence = presence;
    }
    
    //AJOUTER NOUVELLE MÉTHODE
    isPresent(){
        return this.presence;
    }
    
    say(str){
        console.log(this.name + ' : ' + str); // "str" = ce qu'on va définir 
    }
}

user.isPresent();

let user = new User("Anthony", 12, true);
let user1 = new User("Patricia", 22);

console.log(user);
console.log(user1);

//va afficher une petite conversation avec les noms
user.say("Bonjour" + user.name);
user.say("Bonjour" + user1.name);
````

---

### ⚜️HÉRITAGE ENTRE LES CLASSES⚜️

``extends `` = mot clé, "épandre"    
````super```` = mot clé pour invoquer "constructor parent"

- L'HÉRITAGE ENTRE LES CLASSES = créer une hiérarchie de classes
    - une nouvelle classe d'une classe existante = **classe fille**
    - La **classe fille** *hérite* **des propriétés de la classes parente** + peut ajouter / modifier les propriétés

`````js
class Vehicule{
    // class + constructor parent
    constructor(name, wheels){
        this.name = name;
        this.wheels = wheels;
    }
    
    //méthodes
    accelerate(){
        console.log(this.name + " accélère");
    }
    
    brake(){
        console.log(this.name + " freine");
    }
    
    turn(){
        console.log(this.name + " troune");
    }
}

// au lieu de recopier les même proriétés que "vehicule" -> on va utuliser HERITAGE DES CLASSES avec "extends Vehicule ()" devant la classe 
// parce que "car" faut parti la famille des "vehicule"

class Car extends Vehicule(){
    // class + constructor fille
    
    constructor(name){ //recopier les meme valeurs de la propriétés "parent"
       // peut pas invoquer toutes les valeurs dans constructor fille
        
        super(name, 4)// "super" pour invoquer constructor parent
        //mais doit être invoquer ici dans super pour la modifier 
        // 4 = 4 roues dans une voitur "car" par défaut
    }
}

class MotorBike  extends Vehicule(){
    
    constructor(model, hasPasseneger = false){ // model = nom de la propriété
        super (model, 2); // 2 = 2 roues dans une moto
        this.hasPassenger = hasPasseneger; // = 2eme argument ci-dessus
        // toujours mettre new argument après la classe parente
    }
    
}

let car = new Car("Fiat"); // mets que le nom car on a juste invoquer "name"
let moto = new MotorBike("Toyota", true); 

console.log(car);
console.log(moto);

// méthode que j'ai hérité dans la classe "vehicule"
car.accelerate(); // Fiat accélère
moto.turn();// Toyota tourne
moto.brake();//Toyota freine
`````
----

### ⚜️PROPRIÉTÉS ET MÉTHODES "STATIC"⚜️

    Permet de définir une méthode statique d'une classe
    = sont pas disponibles sur les instances d'une classe ≠ sont appelées sur la classe elle-même ( créer / cloner des objets )

- Propriétés et méthodes ne sont pas accessible avec les instances ❌

````js
class Vehicule{
    //PROPRIÉTÉ STATIC
    static GRAVITY = 10; // propriété suivi avec des CONSTANTES EN MAJUSCULE
    //va être appliqué à tous les véhicules
    
    constructor(name, wheels){
        this.name = name;
        this.wheels = wheels;
    }
    
    accelerate(){
        console.log(this.name + "accélère");
    }
    
    brake(){
        console.log(this.name + "freine");
    }
    
    turn(){
        console.log(this.name + "tourne");
    }
    // MÉTHODE STATIC
    static testTechinique(vehicule){ // va prendre vehicule en argument 
        return vehicule.wheels > 0; // retourne la valeur si le vehicule a plus de 0 roues
    }
}

class Car extends Vehicule {
    function

    constructor(name, coffre) {
        super.constructor(name, wheels);
    }
    klaxon(){
        console.log(this.name + "klaxonne");
    }
}

let car = new Car("Fiat", 500);

console.log(car);

car.klaxon();

console.log(Vehicule.GRAVITY, Vehicule.testTechinique(car)); // mettre 'car' en argument pour voir le resultat booléen 
//pour afficher faut mettre la classe concernée ≠ pas aux instances TOUJOURS AFFICHER AVEC LA CLASSE 
````