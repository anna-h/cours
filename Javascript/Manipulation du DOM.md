![dom](../assets/dom.png)

- **DOM** = "Document Object Model", est une interface de programmation pour des documents HTML 
- Transforme un document (page web) sous une forme    
→ Permet au language de script comme JS d'y accéder + manipuler le contenu + les styles  
  - Modification du contenu + style d'un élément précis   
  - Création / suppression d'éléments
  
# SOMMAIRE

- [S'assurer que le Dom est chargé](#sassurer-que-le-dom-est-charg)    
    - [Placer le javascript](#placer-le-javascript)     
    - [Afficher du document](#afficher-du-document)
- [Sélectionner un élément grâce à son id](#slectionner-un-lment-grce--son-id)     
- [Sélectionner des éléments grâce à leurs classes](#slectionner-des-lments-grce--leurs-classes)
- [Sélectionner un ou des éléments](#slectionner-un-ou-des-lments)      
- [Manipuler le style d'un élément](#manipuler-le-style-dun-lment)  
    - [Dynamiser des éléments](#dynamiser)
- [Manipuler les classes d'un élément](#manipuler-les-classes-dun-lment)     
- [Créer et ajouter au dom un élément](#crer-et-ajouter-au-dom-un-lment)     
- [Écouter les évènements du dom](#couter-les-vnements-du-dom)





## ⚜️S'ASSURER QUE LE DOM EST CHARGÉ

---
### ⚜️PLACER LE JAVASCRIPT⚜️

---

![dom-schema](../assets/dom-schema.png)

- **< script > < / script >** = Permet d'écrire du JS dans HTML
- < script type="text/javascript"> 
  console.log("1");
  < /script>
  = Permet de voir console dans le HTML 
  

````html
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    
    <title>Javascript</title>
    
    
</head>

<h1> Javascript </h1>

// PERMET DE LIER FICHIER JS DANS LE FICHIER HTML
//<script src=" " </script> //Nom du fichier JS 

Doit être à la fin du body pour être aperçu ≠ en haut impossible

<script type="text/javascript">
    console.log("1");
</script>
// METTRE A LA FIN POUR MIEUX EXÉCUTER JS

</body>
</html>
````

    La partie <head></head> est d'abord interpréter par le navigateur avant même la page internet

- Pour **lier** du JS dans le fichier HTML ☞ **METTRE A LA FIN DE BODY** pour qu'il télécharge tous les éléments dans le **HEAD** + la structure de la page avant d'exécuter le JS = PAS DE PROBLÈME DE BEUG (pour les navigateurs + navigation)

-----
### ⚜️AFFICHER DU DOCUMENT⚜️

------
- Pour afficher du contenu :
    - Mettre son script à la fin de body pour être interprété
    - ❌ Pas mettre avant contenu = cela affichera **null** dans la console ❌ 
    
✅**Autre méthode si la liaison n'est pas au bon endroit = Utilise "Window"**   

- ````window```` = interface déjà étudié + liée au DOM

````js
// C'EST AFFICHER LE H1 DANS LE JS => POUR LE MODIFIER JUSTE APRÈS 

//SOLUTION N°1
window.onload = function (){
    console.log("Titre");
    console.log(document.querySelector("h1"));
};

//SOLUTION N°2
window.addEventListener("DOMContentLoaded", function() {
    console.log("Titre1");
    console.log(document.querySelector("h1"));
});
````

➡️ Assurer que le DOM est bien exécuter avant de le manipuler 

---

## ⚜️SÉLECTIONNER UN ÉLÉMENT GRÂCE À SON ID⚜️

------
``id`` = Corresponds à la valeur spécifiée d'un objet en argument, Moyen simple d'accéder à un élément particulier + sont uniquement dans un document    
```document.getElementById()``` = Récupère un élément précis dans le JS, (mettre dans une constante) + suivi le nom id


````html
<style>
    .square{
        width: 100px;
        height: 100px;
        background-color: red;
    }
</style>


<div id="sqr" class="square"></div>
````


````js
const square = document.getElementById("sqr"); // RÉCUPÈRE UN ÉLÉMENT PRÉCIS DANS LE JS + LE MODIFIER APRÈS
                    //Element au singulier car sélectionne que id = singuler ≠ pas plusieurs
console.log("square");
````

-   Parce que le javascript va sélectionner tout le fichier HTML → Utilise ça pour sélectionner / modifier un élément précis
----

## ⚜️SÉLECTIONNER DES ÉLÉMENTS GRÂCE À LEURS CLASSES⚜️ 

-----

````document.getElementsByClassName()```` = sélectionne / modifie plusieurs éléments à la fois grâce à leurs classes

`````html
<style>
    .square{
        width: 100px;
        height: 100px;
        background-color: red;
        display: inline-block;
        margin: 10px;
    }
</style>

<div class="square"></div>
<div class="square"></div>
<div class="square"></div>
<div class="square"></div>
`````

````js
const square = document.getElementsByClassName("square");
````

----

## ⚜️SÉLECTIONNER UN OU DES ÉLÉMENTS⚜️

-----

- ````document.querySelector()```` = sélectionne le premier élément dans le document correspondant au sélecteur / groupe spécifié / null si aucune correspondance trouver     

````html
<style>
  .square{
    width: 100px;
    height: 100px;
    background-color: red;
    display: inline-block;
    margin: 10px;
  }
  .bigSquare{
    width: 200px;
    height: 200px;
  }
</style>

<div class="square"></div>
<div class="bigSquare"></div>
<div class="square"></div>
<div class="bigSquare"></div>

<div class="square"></div>
<div class="square"></div>
````
RECHERCHE UN ÉLÉMENT :
- Classe = mettre " . "
- id = " # "
- Balise = ajoute rien X

````js
const square =document.querySelector(".square");
console.log(square);
````

- ````document.querySelectorAll()```` = recherche plusieurs éléments

RECHERCHE DES ÉLÉMENTS : 
- div = " div. "
- Balise = " selection. "

----

## ⚜️MANIPULER LE STYLE D'UN ÉLÉMENT⚜️

MODIFICATION : 

````.style.```` = nom.style.css

````html
<style>
  .square{
    width: 100px;
    height: 100px;
    background-color: red;
    display: inline-block;
    margin: 10px;
  }
</style>
````

`````js
const square = document.querySelector(".square");

square.style.backgroundColor = "blue";

console.log(square);
`````

### ⚜️DYNAMISER⚜️ 

`````setInterval(function(){};````` = mot clé

- Dynamiser la position
````js
square.style.position = "fixed";
square.style.top= "0px";
let position = 0;

setInterval(function (){ //1er argument : fonction callback
    position++; //accrémente (va augmenter)
    square.style.top = position + "200px";
},500);//Timeout = en 1000sec (2sec = 2000)
````

`````js
setInterval(function (){
    position++;
    square.style.top = positionY + "px";
    square.style.left = positionX + "px";
},100);
`````

- Dynamiser la couleur 

````js
let i = 0;

setInterval(function (){
    square.style.backgroundColor = i % 2 ? "red" : "blue"; // si i est impair = red, i = pair = blue
    i = i + 1; // = i++;  = i = i + 1; (same)
},1000);
````
----
## ⚜️MANIPULER LES CLASSES D'UN ÉLÉMENT⚜️ 

---

````classList.add()```` = ajouter   
```classList.remove()``` = retirer

`````js
const square = document.querySelector("square");
console.log(square);

let i = 0;
setInterval(function (){
    if (i % 2){
      square.classList.add("bigSquare"); // va augmenter 
    } else {
        square.classList.remove("bigSquare"); // va rétrécir 
    }
    
},1000);
`````
----
## ⚜️CRÉER ET AJOUTER AU DOM UN ÉLÉMENT⚜️ 

----

````createElement()```` = créer
- div = "div"
- sélection = "selection" 
- lien = "a"
- titre = "h1", "h2", "h3" etc....
- paragraphe = "p"   

``.innerText`` = ajouter du texte / texte déjà présent dans un élément qu'on sélectionne  
```innerHTML``` = insérer du HTML dans un élément  
````appendChild()```` // Déplacer un élément dans une autre page

````js
const square = document.createElement("section");

square.classList.add("square"); //ajoute

square.innerText = "Hello"; // ajoute du texte

square.innerHTML = "<strong>OK</strong>"; // utilise des propriétes HTML entre ("")

document.body.appendChild(square); // Déplace mon élément dans le body 

//Ajoute un container dans body 

let container = document.createElement("div");

container.innerText = "Container"; // ajoute du texte

document.body.appendChild(container); // déplace dans body

container.appendChild(square); // ajoute un élément dans container

 console.log(square);
````
---
##  ⚜️ÉCOUTER LES ÉVÈNEMENTS DU DOM⚜️

 ☞  [Liste de tous les évènements](https://developer.mozilla.org/fr/docs/Web/Events) 

---
`````.addEventListener()````` = Créer un évènement (suivi d'une fonction callback)

`````html
<button id="createSquare">Ajouter un carré</button>

<div id="redSquare">
  <div class="square"></div>
  <div class="bigSquare"></div>
  <div class="square"></div>
  <div class="bigSquare"></div>
</div>

`````


`````js 
window.addEventListener("DOMContentLoaded", function (){
    console.log("DOM CHARGÉ");
    
    
    const btnEL = document.getElementById("createSquare"); // Récupérer le btn (bouton) dans le HTML
    console.log(btnEL);
    
    //POUR AJOUTER UNE ÉCOUTE D'ÉVÈNEMENT 
  
    const redSquareContainerEl = document.getElementById("redSquare"); // Récupérer id de la div du HTML 
  
    btnEL.addEventListener("click", function (){ //Mettre le type (nom d'évènement)+ fonction callback
        console.log("click", this.innerText = "Bouton"); //"this" = référence de mon élément 
      //peut changer de texte avec "this", PEUT FAIRE DES MODIFICATIONS GRÂCE AU "this"
        
        //1er écriture
        const square = createSquare(); // Créer + ajouter un carré // const suivi de la fonction
        console.log(square);
        //2eme écriture
        redSquareContainerEl.appendChild(square); 
        //Mix des deux =
        redSquareContainerEl.appendChild(createSquare());
        
    })
  
})



function createSquare(){ //Créer un élément
    const el = document.createElement("div");
    el.classList.add("square"); // Ajouter une classe square
  return el; // Va retourner cet élement dans la fonction 
    
}
`````