![array](../assets/array-js.png)

----

# SOMMAIRE

- [Array](#array)
- [Créer un tableau](#crer-un-tableau-)
- [Accéder à une valeur grâce à son index](#accder--une-valeur-grce--son-index)
- [Les méthodes pour manipuler des tableaux](#les-mthodes-pour-manipuler-les-tableaux)
  - [La méthode de ".splice"](#la-mthode-de-splice)
  - [La méthode ".forEach()"](#la-mthode-foreach)
  - [La méthode ".find()"](#la-mthode-find)
  - [La méthode ".findIndex()"](#la-mthode-findindex)
  - [La méthode ".map()"](#la-mthode-map)
  - [La méthode "reduce()"](#la-mthode-reduce)
  - [La méthode ".filter()"](#la-mthode-filter)
  - [La méthode ".some()"](#la-methode-some)
  - [La méthode ".every()"](#la-mthode-every)



---
## ⚜️ARRAY⚜️

---

    Tableau = est une structure qui permet d'organiser des valeurs numérotées, peut contenir tout type de données : nombres, strings, objets .... 
    sont avant tout des objets, va pouvoir contenir plusieurs valeurs comme n'importe quel objet 
    → Accessible par l'index : c'est leur position dans le tableau    

```index``` = position de la valeur, commence toujours pas 0, (index = 0 ; valeur = 1)

☞ [Documentation sur les Arrays](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array)

---


## ⚜️CRÉER UN TABLEAU ⚜️

---

````lenght```` = longueur (va être affiché en bas du tableau)   
`````new````` = Permet de créer un nouveau objet   
`````.fill()`````= Remplit tous les éléments d'un tableau vide

`````js
//SYNTAXE 1
const arr = [1, 2, 3]; //Créer un tableau avec des [] = mets des valeurs dedans
const arr = ["Anna", null, 1, true, [2, "1", [9]]]; // peut mettre string, booléen, un autre tableau 

console.log(arr, arr.length); // Va afficher = Index : 0 (gauche) ; valeur : 1 (droite) ;
// .lenght = affiche le nombre de valeurs (ici lenght: 3)

//SYNTAXE 2
const arr1 = new Array(10); //Créer un tableau vide = new + Array(( nombre longueur du tableau ) si ce dernier donne juste un nombre = longueur)
// ici lenght = 10
const arr2 = new Array(10, "Anna", 9 ); // Là va mettre toutes les valeurs ≠ longueur (1 nombre)

arr1.fill(89);// Ramplit le tableau vide avec la valeur 89 

console.log(arr1);
`````

- **2 SYNTAXES** : 
    - **[ valeur, valeur, valeur ];** = Créer un tableau avec des valeurs
    - **new Array().fill();** = Créer un nouveau tableau vide + ajouter une valeur plusieurs fois

``array.push(valeur)`` = Ajoute un / plusieurs

````js
const animals = ["cat", "dog", "rabbit"];

const count = animals.push("lion"); //va ajouter une valeur en plus au tableau 
console.log(count);
// va afficher : animals ["cat", "dog", "rabbit", "lion"];
````


----
## ⚜️ACCÉDER À UNE VALEUR GRÂCE À SON INDEX⚜️

---

``arr[ index ] =`` Voir la valeur grâce à la position de l'index

`````js
const arr = ["Anna", "Tony", 9, true];

console.log(arr, arr[1], [7, 8, 9].length); // arr[1] : va afficher la valeur de la position INDEX 1 = "Tony"
// ([7,8,9].length); = Pour savoir la longueur de ce tableau qu'on vient de créer dans console

console.log(arr, [arr.length - 1]); // C'est pour voir l'avant-dernièr valeur du tableau ( quand le tableau possède plusieurs valeurs +++ )
`````

----
## ⚜️LES MÉTHODES POUR MANIPULER LES TABLEAUX

----

`````
Plusieurs méthodes utilisent des fonctions comme argument.
→ Traiter chaque élément du tableau
→ Evolue longueur du tableau + traite élément dont l'indice est inférieur à la longueur
= Autre modificatons apportées au tableau : peut avoir impact sur traitement des éléments suivants

⏩ Donc défini plusieurs méthodes , pour pas complexifier le code lorsqu'on modifera tableau = créer une copie avant d'invoquer une méthode
`````
---

## ⚜️LA MÉTHODE DE ".splice"⚜️

`````.splice````` = Ajoute de nouveaux éléments, Modifie le contenu d'un tableau en retirant des éléments, Supprime / Remplace une partie d'un tableau

````js
const arr =[];

//Insérer des données
arr.push("Anna", "Tony", "Patricia");
console.log(arr); 

//Supprimer des données
arr.splice(0, 1);// start: index de la donnée, deleteCount: la valeur // va afficher Array ["Tony", "Patricia"]
arr.splice(1, 2);// va afficher : Array ["Anna"] , car supprime la position index : 1 + deleCount: 2 données à partir de la position index
arr.splice(-1, 1); // va afficher : Array ["Anna", "Tony"] // supprime le dernier élément
console.log(arr);
````


-----
### ⚜️LA MÉTHODE ".forEach()"⚜️

---

```` .forEach()````  = Permet d'exécuter une fonction donnée par chaque élément du tableau

````js
let arr = [1, 2, 3, 4];

for (let i = 0; i < arr.length, i++;){
    arr[i];
}
````

➡️ Peut traverser de la manière


```arr.forEach(function (value:number, index:number, array:number[]) { }); = Utilise une fonction callbacl```

````js
let arr = [1, 2, 3, 4];

arr.forEach(function (value:number, index:number) {  // =argument (dans la fonction)
    console.log(index, value);
}); 

// va afficher l'index (à gauche) puis à côté value (à droite)
````

↪️ La fonction callback ne retourne aucune donnée/ valeur = undefined ❗️  
❗️ La méthode forEach ne retourne aucune donnée/ valeur = undefined ❗️


-----
### ⚜️LA MÉTHODE ".find()"⚜️

---

````` .find()````` = Permet de trouver un élément dans un tableau en fonction d'une condition

````js
let arr = ["Anna", "Antony", "Bibi", "Tony"];

// peut être stocker dans une variable
let callback = function (name, index){

    console.log(index, name); // index commence toujours par 0 (par défaut) 
    return false; //retourne faux pour traverser le tableau

    //condition avoir lettre "o" dans une valeur
    
    if (name.includes("o")){
        return true;      // va retourner true si a "o" dans la valeur 
    } else {
        return false;    // si false va continuer la méthode "find"
    }
};
//arr.find(predicate: function () {}); = soit peut être défini dans l'argument 

arr.find(callback); // continue de chercher (= find)
console.log("res:", res);
````

✅ SIMPLIFICATION ✅

`````js
let arr = ["Anna", "Anthony", "Bibi", "Tony"];

let callback = function (name){ // fonction défini par une variable
    return name.includes("o"); // pas obliger d'utliser un booléen car : si contient lettre "o" donc  va afficher ≠ sinon va chercher d'autre élément (= find)
};

let res = arr.find(callback);
console.log("res:", res);

//SOIT 

let res = arr.find(function (name) {  //fonction va passer en argument
    return name.includes("o");
});
console.log("res:", res);
`````
----
### ⚜️LA MÉTHODE ".findIndex()"⚜️

----
````` .findIndex()````` = Permet de retourner l'index d'un élément qui respecte la condition

- Ressemble à la méthode "find", *ne va pas retourner la valeur qui respecte la condition* ≠ mais **va retourner son index**

- Va être plus utiliser que la méthode "find"

````js
let arr = ["Anna", "Anthony", "Bibi", "Tony"];

//invoque une fonction pour chaque valeur jusqu'à corresponds à notre conditon 
let callback = function (element, index){
    
    console.log(element, index);
    
    return element.length === 7; // condition : égale à 7
    // dès que la condition est respectée : la méthode findIndex va s'arrêter + retourne index

    return element.length === 4; // condition : égale à 4
    // va prendre la première valeur si contient plusieurs valeurs égale à 4 
};

let index = arr.findIndex(callback);

console.log("Index :", index, arr[index]); // peut voir la valeur de l'index qui est stocker avec : "arr[index]"
````
----
### ⚜️LA MÉTHODE ".map()"⚜️

----

````` .map()````` = Permet de modifier + créer un nouveau tableau

**⭕️Pour tableau avec nombre**

`````js
let arr = [1, 2, 4];

let callback = function (value, index){
    
    console.log(value, index);
    
    return value * 2; 
    // va retourner la valeur avec résultat par * 2 ( 1*2=2, 2*2=4, 4*2=8, etc...)

    return value % 2 ? "Impair": "Pair";
    // va retourner si la valeur est un nombre pair ou impair
    
    return value % 2 ? "impair : " + value: "pair : " + value;
    //utilise ternaire : affiche la valeur + valeur condition (pair / impair)
};
let arr2 = arr.map(); // va retourner un nouvrau tableau


console.log(arr2);
`````

**⭕️Pour objet**

````js
let arr = [
    {age: 21},
    {age: 29},
    {age: 12},
];

let callback = function (value, index){ // value : objet (age) // va afficher {age: 21}, etc...
    console.log(value);
    
    return value.age; // va afficher index + valeur 
};
````
----
### ⚜️LA MÉTHODE ".reduce()"⚜️

---

````` .reduce()````` = Permet de réduire un tableau à une certaine valeur / ou sur une autre forme

`````js
let arr = [3, 79, 89]; // veut réduire tableau de nombre  en un nombre qui sera la somme cumulé

let accumulation = arr.reduce(function (acc, value, index){ 
    
    console.log(acc, value, index);
    
    acc = acc + value; //peut modifier l'accumulateur
    //valeur = 3 + 0 (valeur initiale) = 3 , retourne = 3 = acc
    //acc = 3 + valeur 79 = 82 , retourne = 82
    //acc = 82 + valeur 89 = 171 , retourne = 171
    
    return acc; // 1er fois vaut la valeur initiale = 0 en bas 
    // puis les autres valeurs valent valeur retourner
    
}, 0);// = valeur intiale = première ligne

console.log(accumulation, arr); // va afficher en tableau + la somme 

`````

```accumulateur``` = **acc** (abréviation) = donnée qui va passer entre chaque fonction  
→ prends la valeur qui est retourné par la fonction précédente

**⭕️Créer un tableau dans un tableau**

````js
let arr =[3, 79, 89];

let accumulation = arr.reduce(function (acc, value, index){
    console.log(acc, value, index);
    
    acc.push(value); // push valeur dans un tableau = chaque valeur va être dans un tableau
    
    return acc;
}, []);

console.log(accumulation, arr);
````

---
### ⚜️LA MÉTHODE ".filter()"⚜️

---

`````` .filter()`````` = Permet de filter des éléments dans un tableau en fonction d'une condition


`````js
let arr = [2, 3, 19, 73, 89];

let arrFiltered = arr.filter(function (nb, index){
    console.log(nb, index);
    
    //retourne à un booléen
    if (nb % 2){   // si le nombre est impair
        return true; // donc retourne true
    } else{
        return false;
    }
    
});

console.log("array filter:", arrFiltered); // va afficher que les retours vrais
`````


✅SIMPLIFICATION✅

````js
let arr = [2, 3, 19, 73, 89];

let arrFiltered = arr.filter(function (nb, index){
    console.log(nb, index);
    
    return nb % 2; // 1 équivalent de TRUE // 0 équivalent de FALSE
    // si le nombre est IMPAIR = restera 1 = true // si le nombre est PAIR = restera 0
    
    return !(nb % 2); // ! = inverse
    // inverse de TRUE = retourne FALSE 
});

console.log("array filter: ", arrFiltered);
````

----
### ⚜️LA METHODE ".some()"⚜️

---


`````` .some()`````` = Permet de renvoyer **true** si **au moins un élément** du tableau correspond à une condition ≠ cas contraire (si la valeur corresponds pas à la condition = **false**)


````js
let arr = [2, 3, 19, 73, 89];

let cond = arr.some(function (value, index) { 
    console.log(value, index);
    
    if (value < 10){ //tester si valeur inférieur à 10
        return true;
    } else{
        return false;
    }
    
    //SIMPLIFICATION
    
    return value < 10;
});

console.log(cond); // pour voir si ma condition est vrai ou non
````
___
### ⚜️LA MÉTHODE ".every()"⚜️

---

`````` .every()`````` = Permet de retourner vrai si toutes les valeurs respectent une condition ≠ cas contraire (si la valeur corresponds pas au condition = **false**)

````js
let arr = [2, 3, 19, 73, 89];

let cond = arr.every(function (value, index) {  // il faut que toutes la valeurs soient TRUE pour être affiché ≠ si FALSE n'affiche rien du tout 
    console.log(value, index);  // affiche qu'une valeur car il faut retourner
    
    return true;
    //Doit afficher TOUTES LES VALEURS = TRUE 
    
    return value > 0;
    //Ma condition est vrai = TRUE = affiche ma fonction
});

console.log("Toutes les valeurs:", cond); // vérification 
````

⚠️ TOUTES VALEURS DOIT CORRESPONDRE À LA CONDITION POUR ÊTRE AFFICHÉ ⚠️    
➡️ Même si une valeur ne corresponds pas à ma condition = FALSE 
