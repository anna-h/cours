![sequelize- logo](./assets/sequelize-logo.png)

        Sequelize = ORM : (Object Relational Mapping) est un ensemble de classe permettant de manipuler les tables de données relationnelles (objets), c'est une interface d'accès à base de données  qui donne illusion de ne plus travailler avec des requêtes SQL, mais de manipuler des objets
        Pas besoin de se soucier de base de données, c'est l'ORM qui prends la charge de rendre compatible au base de données
        → va s'appuyer de modèle= table (de la base de données)

        Sequelize : est un ORM basé sur les promesses, compatible avec mysql + avec d'autres systèmes de gestion de base de données 
        Permet de connecter à notre API à nos base de données MySql, pour stocker une liste d'utilisateurs (ajouter / modifier/ supprimer ) des utilisateurs + récupérer des données
       
⚠️ 1 exemple tout le long du cours, donc chaque code se suivent ⚠️ 

# SOMMAIRE
- [Installation](#installation)
- [First Connexion](#first-connexion)
- [Définir le 'model user'](#dfinir-le-model-user)
- [Synchronisation des modèles](#synchronisation-des-modles)
- [Enregistrer un utilisateur](#enregistrer-un-utilisateur-dans-la-base-de-donne)
- [Récupérer les utilisateurs sauvegarder en base](#rcuprer-les-utilisateurs-sauvegarder-en-base)
- [Trouver un utilisateur grâce à sa clé primaire](#trouver-un-utilisateur-grce--sa-cl-primaire)
- [Concept "Foreign Key"](#concept-foreign-key-cl-trangre)
- [HasMany](#has-many)
- [Manipuler les relations](#manipuler-les-relations)

## Installation

![installation](./assets/sequelize-installation.png)

- Installation du driver my-sql

![installation driver](./assets/sequelize-installation-driver.png)

## First Connexion 

````javascript
//fichier server.js
const express = require('express');
const cors = require('cors');

//récupérer  la class sequelize qui se trouve dans la const sequelize => const Sequelize.Sequelize = require('sequelize') //au lieu de écrire 2fois la même chose ☞
const {Sequelize} = require('sequelize'); //Destuction l'objet + récupérer la classe

//⭕️Créer la connexion à MySql 

//donner en 1er params la base de donnée ('app')
// 2nd params l'utilisateur avec lequel je vais me connecter ('root')
//3rd params donner le mot de passe ('Rootroot)
// Dernier arguments c'est des options ('{dedans ce sont des éléments 'host', 'dialet'}')

const sequelize = new Sequelize('app', 'root', 'Rootroot', {
    host: '127.0.0.1',
    dialect: 'mysql'
})

//⭕️ Tester la connexion à Mysql

sequelize.authenticate().then(() =>{ //renvoie une promesse 'then' , peut réagir à la promesse avec la fonction callback
    console.log('Connection sucessfull');
}).catch((error) =>{ //réagir à l'erreur
    console.error('Unable to connect to the database', error); //va afficher l'erreur
});
````

**Vérification** :          
![vérification](./assets/Sequelize-first-connexion.png)

---
## Définir le "model user"

`model (dans sequelize)` : la table

````javascript
//fichier server.js
sequelize.define(modelName: 'User', attributes: { //atttributes = colonnes
    firstname:{
        type: DataTypes.STRING, //c'est un varchar = 'DataTypes', de type 'string'
        allowNull: false
    },
    lastname: {
        type: DataTypes.STRING,
        allowNull: false    
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique; true,
    },
    note: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0
        }
    }
}, options:{
     sequelize, //sequelize: sequelize, //la clé : 'sequelize' dont la valeur = à la variable 'sequelize' ⬆️
     tableName: 'users'
    //c'est pour désactiver les colonnes
     createdAt: false,
});
````

- Détail :
    - L'instance de la connexion = sequelize
    - Méthode `.define` : pour définir
    - 1er argu = le nom du model 'User'
    - 2nd argu = tout la config des colonnes
    - 3rd argu = donner des options
    
- Configuration des colonnes basé sur ça :            
![model user](./assets/sequelize-model-user.png)
  
---

## Synchronisation des modèles

    Permet de synchroniser le modèle avec la table à laquelle est synchronisée

- ⭕️ Synchronisation le model User
````javascript
//fichier server.js
//utilise des fonctions fléchées
User.sync({ //contient objet
    alter: true,
})
    //contient de opotions
    .then(()=>{
        console.log('Sync User success');
    })
    .catch((err) =>{ 
        console.log('Sync User error :', error);
    });
````

Quand ne met aucune option = cela va créer la table si elle n'existe pas, et si elle existe déjà cela ne va rien faire       

`force`: est un booléen, va écraser la table déjà existante + en créer une nouvelle table 
≠
`alter`: va vérifier l'état actuel de la table de la base de donnée

- On peut vérifier dans l'application mysql ⬆️             
![vérification mysql](./assets/sequelize-synchronisation-des-modeles.png)
  
Control + C & node server.js = refresh la page + envoie les données dans l'application mysql

- ⭕️ Synchronisation tous les models (plusieurs tables)
```javascript
//fichier server.js
sequelize.sync({alter: true}).then(()=>{
    console.log('Sync User success');
})
    .catch((error) =>{
        console.error('Sync User error:', error)
    })
```

---

## Enregistrer un utilisateur, dans la base de donnée

```javascript
//fichier server.js
//vérification avce insomnia
//POST 
server.post('users',(request, response) =>{
    User.create({
        firstname: request.body.firstnName,
        lastname; request.body.lastName,
        email: request.body.email
    });
    
    response.send({message: 'Utilisateur enregistré !', data: request.body});
});
```

Faire la modification directement sur insomnia + envoie dans mysql              
![enregistrer un utilisateur](./assets/sequelize-enregistrer-un-utilisateur.png)
![enregistrer un utilisateur](./assets/sequelize-enregistrer-un-utilisateur1.png)

---

## Récupérer les utilisateurs sauvegarder en base

````javascript
//fichier server.js
//code de base
server.get('/users', function (request, response){
    const users = [
        {id: 1, firstName: 'Jean'},
        {id: 2, firstname: 'Sam'},
        {id: 3, firstname: 'Eli'},
    ];
    response.send(users);
})
````

`findAll()`: récupère tous les éléments dans la table

````javascript
//fichier server.js
//ce qu'il faut faire✅
server.get('/users', function (request, response){
    User.findAll().then(()=>{
        response.send(users);
    }).catch((err)=>{
        response.statutsCode = 404;
        response.send('Liste introuvable' + err);
    });
});
````

![récupère les utilisateurs](./assets/sequelize-recupere-les-utilisateurs.png)

- **Ajouter des attributs**
````javascript
server.get('/users', function (request, response){
    //ici la recherche
    User.findAll({
        //⭕️Attributs
        attributes: ['id', 'firstname']
        //⭕️limite
        limit: 2,
        //⭕️where
        where:{
            firstname: 'Steve'
        }
    })
    //promesse
      .then(()=>{
        response.send(users);
    }).catch((err)=>{
        response.statutsCode = 404;
        response.send('Liste introuvable' + err);
    });
});  
````
- **Attribut**
![recupere les utilisateurs](./assets/sequelize-recupere-les-utilisateurs-2.png)

- **Limit**
![limit](./assets/sequelize-recupere-les-utilisateurs-limit.png)
  
- **Where**
![where](./assets/sequelize-recupere-les-utilisateurs-where.png)
  
---

## Trouver un utilisateur grâce à sa clé primaire

[☞ Liste de tous les méthodes Sequelize](https://sequelize.org/master/manual/model-querying-finders.html)

````javascript
//fichier server.js
server.get('/users/:id', function (request, response){
    
    User.findByPk(request.params.id)//cette méthode => effectuer une recherche
        .then((user) =>{ //quand récupère l'utilisateur
            console.log(user);
            if(!!user){ //condition : si l'utilisateur n'est pas null ou undefined = donc a bien retrouvé l'utilisateur grâce à l'id => donc va renvoyer une réponse avec les infos de l'utilisateur
                response.send({user}); //donc renvoie les infos de l'utilisateur
            } else {  //dans le cas contraire
                response.statutsCode = 404;
                response.send({message: 'Aucun utilisateur trouvé'}); //va renvoyer ce message si retrouver grâce à l'id
            }
        })
        .catch((err) =>{
            response.statutsCode = 404;
            response.send('Une errreur est survenue :' + err);
        });
});
````

`findByPk` : trouver par la clé primaire (Primary Key)

On récupère bien id + toutes ses infos (mysql) dans l'application insomnia dans mon URL            
![trouver un utilisateur grâce à sa clé primaire](./assets/sequelize-trouver-un-utilisateur-grâce-à-pk.png)

![documentation](./assets/sequelize-doc-findall.png)


---

## Concept "Foreign Key" (clé étrangère)

MySQl prends en charge les clés étrangères qui permet le croisement de données lier entre les tables & les contraintes de clés étrangères qui aident à maintenir la cohérence de données associer

Une relation de clé étrangère implique une table parent qui contient des valeurs de la colonne initiale, une table enfant avec des valeurs des colonnes qui référence les valeurs de la colonne parent

La contrainte de la clé étrangère est défini sur la table enfant :                  
![foreign key](./assets/sequelize-foreign-key.png)                    
**users** est la table parent, **posts** est la table enfant stockera la **clé étrangère** et aura la valeur de l'id de l'utilisateur auquel est associé sur la table users

_Exemple_ :                     
![exmple](./assets/sequelize-foreign-key-exemple.png)

- Contrainte : 
  - **RESTRICT** : **Empêche la suppression** d'un utilisateur tant qu'il existe une clé étrangère faisant référence à cet utilisateur.   
    Pour pouvoir supprimer l'utilisateur, il faudra **supprimer tous les enregistrements de la table post** qui auront l'id de l'utilisateur supprimé en tant que valeur de la colonne users id
  - **CASCADE** : Si on supprime un utilisateur, tous les articles associés à l'utilisateur seront supprimé en **cascade**.    
    Ni les utilisateurs ni les articles dont il est l'auteur ne seront encore présent après la suppression de l'utilisateur   
  - **SET NULL** : Dans le cas où on supprime un utilisateur, tous les articles qui lui sont associés vont **d'être mise à jour**.        
    Leur **valeur users id passera à null**. Les posts ne seront pas supprimer mais n'auront plus d'auteur
  - **NO ACTION** : C'est l'équivalent de **restrict** le **serveur mysql rejette l'opération de suppression** ou de **mise à jour de la table parent**, s'il existe une valeur de clé étrangère associée dans la table de référencement
  - **SET DEFAULT** : va **SET la valeur** de users id avec la valeur par défaut de la colonne users id
  
➡️ Ces contraintes sont associées à des actions : 

- Actions :
  - **ON UPDATE** : Permet de **déterminer le comportement mysql** en cas de **modification** d'une référence
  - **ON DELETE** : Permet de **déterminer le comportement mysql** en cas de **suppression** d'une référence
  
---

## "has many"

     HasMAny : association un à plusieurs

- Déclarer le modèle Post, columns : title, text
````javascript
//fichier server.js
const Post = sequelize.define(modelName: 'Post', attributes: {
    title: {
        type: DataTypes.STRING,
        allowNull: false
    },
    text:{
        type: DataTypes.TEXT,
        allowNull: false,
    }
}, options: {
    sequelize,
    tablNAme: 'posts',
    createdAt: false,
    updatedAt: false,
});
````

![has many post](./assets/sequelize-has-many-post.png)

Un utilisateur peut avoir plusieurs posts

`as` : c'est pour renommer       
`source`: référence

- Définir la relation : User has many Post
````javascript
//fichier server.js (à la suite)
User.hasMany(Post, options: {
    as: 'posts',
    foreignKey: 'fk_author_id',
    sourceKey: 'id',
    constraints: true,
    onDelete: 'cascade',
})
````

`include` : récupère un modèle précis de nos choix

````javascript
// récupère tous les post de mon utilisation
server.get('/users', function (request, response){
    User.findAll(option:{
        include: [{model: Post, as:'posts'}]
    })
      .then((users)=>{
          response.send(users);
      })
      .catch((err)=>{
          response.statusCode = 404;
          response.send('Liste introuvable :' + err);
      });
});
````
![recup](./assets/sequelize-has-many-insomnia.png)

--- 

## Manipuler les relations

- Créer un nouveau _post_ à chaque requête & l'associé à l'utilisateur 
````javascript
server.get('.users/:id', function (request, response){
    
    User.findByPk(request.params.id)
            .then(async(user)=>{
                if(!!user){
                    //⭕️Créer un nouveau post à chaque requête
                    const post = await Post.create({
                      title: 'Title',
                      text: 'Text',
                    })
                  //⭕️ Add - Associé à l'utilisateur
                  //écrire de nom de 'as' (posts: nom du modèle)
                  awit user.addPost(post);
                  const posts = await user.getPosts(); //await: attends pour retourner une réponse
                  //donc va récupérer la liste des posts (grâce à la const)
                 
                  //⭕️ Set
                  //écrire de nom de 'as' (posts: nom du modèle)
                  awit user.setPosts([post]);

                  //⭕️ Remove 
                  //écrire de nom de 'as' (posts: nom du modèle)
                  awit user.removePosts([post]);
                  
                  response.send({user, posts});
                });
            }else{
                response.statutsCode = 404;
                response.send({message: 'Aucun utilisateur trouvé'});
})
})
````

`async` : renvoie systématiquement une promesse      
`await`: ne peut être utilisé que dans la fonction **async**, Permet d'attendre la résolue d'une promesse et retourner sa valeur            
`.set` : écrase les valeurs + fait une mise à jour

![manipuler les relations](./assets/sequelize-manipuler-les-relations.png)
![manipuler les relations mysql](./assets/sequelize-manipuler-les-relations-mysql.png)


