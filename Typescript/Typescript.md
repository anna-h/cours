![ts-logo](../assets/tp-nodejs.png)

# SOMMAIRE

- [NodeJS](#node-js)
- [NPM](#npm)
- [Typescript](#typescript)
- [Package.json](#configurer-un-package-nodejs)
- [Basic types](#basic-types)
- [Functions](#les-fonctions)
- [Enums](#enums)
- [Literal Types](#les-types-littraux)
- [Types Assertions](#types-assertions)
- [Interfaces](#interfaces)
- [Class](#les-classes)
- [Parameter Properties](#parameter-properties)
- [Import Export Modules](#import-export-modules)

## ⚜️NODE JS

    C'est un moteur d'exécution du language Javascript

## ⚜️NPM 

    C'est l'abréviation Node Packet Manager 
    Un paquet est un programme qui est développer en Javascript + est mis en avant sur la plateforme NPM
    Installer : programme, librairie, framework, parquet qui sont mis sur le site NPM
    → Peut installer, créer, gérer + publier nos propres paquet (React, Angular, JS, ...)
    Outil de distribution du code 

## ⚜️Typescript⚜️

    C'est un language de programation qui est développé par Microsoft ( gartuit ) 
    Va ajouter un ensemble de fontionnalité à Javascript : typage 
     → Va rendre le code plus lisible, propre + améliorer + sécurisé le code
    Pour lire du typescript transpiller en Javascript ( transpiller = logiciel qui va permettre de traduire un code vers à un code cible )


- INSTALLER TYPESCRIPT 

[Lien Typescript](https://www.typescriptlang.org/#installation) 

![img.png](../assets/tsc-inst.png)


### ⚜️TRANSPILER⚜️

- Copier le premier lien dans la terminal ( -g = installer de manière global sur tout notre ordinateur ) 
````npm install -g typescript````
  
UTILISER LE TRANSPILER POUR TRADUIRE EN JS

- Deuxième lien : copier ````tsc```` + le nom du fichier .ts ; dans la terminal 

---
## ⚜️CONFIGURER UN PACKAGE NODE.JS⚜️

---

- **CRÉER UN package.json** 

``sudo npm i typescript`` + ajoute mot de passe du pc = Installer typescript sur Mac    
``npm --init`` = va créer le package.json   
++ va ajouter un fichier .js en dessous du fichier .ts

---
Paquet = dossier / projet

````npm init```` = Mot clé pour créer le package.json  
`````package.json`````= qui contient tous les infos qui contient dans mon paquet

_Exemple_ : 

````json
{
"name" : "foo", 
"version" : "1.0.0",
"description" : "A package for fooing things",
"main" : "foo.js",
"keywords" : ["foo", "fool", "foolish"],
"author" : "John Doe",
"licence" : "ISC"
}
````
 
➡️ + Va rajouter un fichier .js du même nom (transpiler)

---
## ⚜️BASIC TYPES⚜️

---

- **⭕️NOMBRES**
````ts
let a = 3;  // Interpréteur va assigner le type de la valeur à la variable = TYPAGE DYNAMIQUE ( DYNAMIC TYPE )
a = "2"; // donc va afficher "2" : une string , parce que va prendre la dernière valeur
````

✅ CE QU'IL FAUT FAIRE ✅
    
`````ts
let a: number = 3; // va préciser a : un nombre ≠ pas autre chose 
// a = "4"; = ERROR 
`````    

- **⭕️STRING**

```ts
let str: string;
str = "Hello";
```

- **⭕️BOOLÉEN**

````ts
let bool: boolean = true;
bool = false;
````

- **⭕️ARRAY**

````ts
let list: number[] = [1, 2, 3];
//Ou
let list1: Array<number> = [1, 2, 3];
````

- **⭕️TUPLE** : Doit un tableau avec un nombre + un autre élément 

````ts
let x: [string, number];

//Initialise

x = ["Hello", 89]; // ✅ VRAI

x = [10, "Hello"]; // ❌ ERROR
````

- **⭕️COMPILER PLUSIEURS ÉLÉMENTS**

````ts
let nb: [] | string | boolean;
nb = true; "Hi"; [1, 2];
````

- **⭕️OBJET**  : ````number````, ``string``, ``boolean``, ``null``, ``undefined``, ``symbol``

````ts
let obj: {name: string, age: number};
obj = {name: "Anna", age: 21};
````

``age?: number`` = ? cet élément est optionnel (mettre un point d'interrogation après l'élément)

---
## ⚜️LES FONCTIONS⚜️

---

````void```` = rien   
`````=>````` = va retourner

`````ts
//SYNTAXE 1
function mutiply(nb1: number, nb2: number): number{ // ":" va récuper (ici un nombre)
    return nb1 * nb2;
};

//SYNTAXE 2
let mutiply1: (nb1: number, nb2: number) => void= function (nb1, nb2){ // Déclare une variable 
    //Variable prend qu'une fonction ≠ pas d'autre ce qu'il y a dans les paranthèses () après la variable
    // Donc va rien retourner avec le mot "void"
};

let mutiply2: (nb1: number, nb2: number) => number = function (nb1, nb2){ // => : va retourner  
    return nb1 * nb2; // retourner un nombre ( "=>" )
}

mytiply(1, 2);
mutiply1(1, 2);
mutiply2(1, 2);
`````


_Exemple_ : 

```ts
function createUser(name: string, age: number, address?: string)
    //TYPER une variable
    {name: string, age: number, address: string }{
    
    
    return{
        name: name, 
        age: age,
        address,
        // PAREIL : SI les éléments ont le même noms = écrit directement les éléments
        name,
        age,
        address
        
    };
}
//Recevoir une fonction
let fn : (nb : string) => boolean | number;

createUser( "Anna", 21);
```

---

## ⚜️ENUMS⚜️

    Enums = énumérateur

```ts
// créer un type
enum Direction {
    //constantes // peut ne pas mettre des valeurs => valeur par défaut = 0 et ainsi de suite
    TOP = "123", // 0
    BOTTOM = "327", // 1
    RIGHT = "567", // 2
    LEFT = "234", // 3
}
Let userDirection : Direction = Direction.BOTTOM; //mettre une des constantes

//teste la correspondante de la valeur
if (userDirection === Direction.BOTTOM){
    console.log('tu montes');
}

console.log(userDirection);
```

![enums](../assets/typescript-enums.png)



---
## ⚜️LES TYPES LITTÉRAUX⚜️ 

---

     A le même syntaxe que les types littéraux de chaînes de caractères en Javascript 
    Permet de créer de nouveaux noms de propriétés basés sur les anciens 

````ts
let var: 1 | 2 | 3 | 4 | 5;

var = 4; //✅  à dans ma variable 

var = 7;//❌ n'existe pas dans ma variable
````

---
## ⚜️TYPES ASSERTIONS⚜️

---

``as`` = modifier le type de la variable

````ts
let input: unknown = "73";
let var1: string;

var1 = input: string; // ❌ ERROR , car var1 n'est pas égal à input

//MODIFIER LE TYPE DE LA VARIABLE

//SYNTAXE 1
var1 = input as string;

//SYNTAXE 2
var1 = <string> input; //mettre en guillemets va casser le type de la variable
````
----
## ⚜️INTERFACES⚜️

---

    C'est un contrat de structure pour nos objets  
    Mettre un " I " devant le nom d'une interface pour ne pas confondre avec une classe
``interface`` = mot clé

_Exemple_ : 

````ts
interface PersonId{
    name: string;
    color: string;
    age: number;
}

// ❗️ METTRE TOUS LES VALEURS COMME L'INTERFACE SINON ERROR ❗️
const user1: PersonId = {
    name: "Anna";
    color: "red";
    age: 21;
};

function hello(perso: PersonId){
    console.log("Bonjour à toi ${person.name} et tu as ${person.age} ans."); // ${} = prends les éléments de la variable
    //va afficher ( Bonjour à toi Anna et tu as 21 ans.) 
    //va prendre les infos dans la variable, car a deja défini les valeurs 
    //interface = contrat , respect élements + ajoute valeurs dans les éléments 
}

````


- ⭕️ **POUR UNE FONCTION**

````ts
interface MyFn{
    tick(): boolean; // nommer ma fonction 
}

let myFn: MyFn = 
    tick : function (){ // mettre en objet pour nommer une fonction 
    return true;
};

myFn.tick();
````

- ⭕ ️**POUR TABLEAU**

`````ts
interface RandomArray{
    [index: number] : string; // veut typer index
}

let arr: RandomArray = [
    "Ma valeur"
];
;
console.log(arr[0]);
`````

- ⭕️ **POUR UN HÉRITAGE**


`````ts
interface IVehicule{
    model: string;
    move(): void;
}

interface ICar extends Vehicule{ // interface car étends interface vehicule
    model: string;
    wheels: number;
}

let car: ICar = {
    wheels: 4,
    model: "Fiat"
    move: function (){
    }
};
`````
---
## ⚜️LES CLASSES⚜️ 


☞ [Documentation sur les classes Typescript](https://www.typescriptlang.org/docs/handbook/classes.html)

--- 
    Les classes offrent une structure abstraite
    Créer des composants réutilisable
    Permet d'utiliser des classes à la place des formats des frameworks (embrerhs, reactjs, etc..)


```class``` = mot clé 

 **Mot clé pour HÉRITAGE**  :  
````public```` : est toujours défini par défaut, peut accéder à tous les propriétés  
```protected``` : C'est un cas d'héritage, est qu’accessible que par les enfants    
````private````: Personne ne peut accéder si n'a pas été déclaré par la propriété


````ts
//PARENT
class Car {
    //TSC = veut typer des éléments, donc faut définir dans la classe
    // AJOUTE DES MOTS CLÉ POUR LES HÉRITAGES
    public name: string;
    protected km: string;
    pirivate
    model: string; // Déclarer la propriété avant de l'enregitrer (constructor)

    constructor(model: string) { // vouloir récupérer élément/propriété
        this.model = model; // enregistre dans une instance
    }

    // HÉRITAGE
    //faut CRÉER DES MÉTHODES POUR AVOIR ACCÈS AU HÉRITAGE

    move() { // Méthode ( instance )
        console.log(this.model + "moves"); // concaténation 
    }

    //cas de "PRIVATE"
  public getModel(): string{
        return.this.model
  }
    
}
//ENFANT
class BMW extends Car { //sous classe de voiture 
    //BMV extends de l'enfant de la classe "Car"
    constructor(model: string) {
        super(model);// va exécuter le constructor parent
      console.log(this.km);
      
      //pour afficher héritage "private"
      console.log(this.getModel());
    }

}

let car = new Car(model
:
"Fiat"
)
; // instance attends la propriété + donner la valeur

console.log(car.model);
````


**⭕️DÉCLARER UNE INTERFACE**

````ts
//Contrat
interface ICar{
    name: string;
    model: string;
    move(): void;
}

class Car implements { // va implémenter/ installer le contrat défini par "Icar" (ici)
    name: string;
    model: string;
    move() void {
    }
}

let car: ICar = new Car();// créer une instance
car.name;
````

- **⭕️TYPAGE D'UNE CLASSE**

````ts
interface Icord {
    x: string;
    y: string;
}

let cord: Icord;

cord = {
    x: "ok";
    y: "ok";
}
````

✅CE QU'IL FAUT FAIRE✅

````ts
var cord;
cord = {
    x: "ok";
    y: "ok";
}
````

_Exemple_ : 

````ts
class Point {
    x: number;
    y: number;

    constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
    }
    add(point: Point){ //ajoute 
        return new Point(this.x + point.x, this.y + point.y);
    }
}

var p1 = new Point(0, 10);
var p2 = new Point(10, 20);
var p3 = p1.add(p2); // {x:10, y:30} , concaténation
````
---
## ⚜️PARAMETER PROPERTIES⚜️

    Permet de créer + initialiser une propriété en un seul endroit

---

````ts
class Person {
    public name: string;
    
    constructor(name) { 
        this.name = name;
    }
}

// ✅SIMPLIFICATION✅

class Person {
    constructor(public name: string, public  age: number) { //Déclare + initialise les propriétes au même endroit
        // En utilisant les mots clés d'héritage
    }
}
// TYPAGE
let person: Person = new Person("Anna", 21);
console.log(typeof person, person.name); // va afficher le type ( ici = c'est un objet  + "Anna") 
````
---
## ⚜️IMPORT, EXPORT MODULES⚜️

---

    MODULE = sont exécutés dans leur propre étendue, pas dans la portée globale : variable, fonctions, classes etc... 
    Un module ne sont pas visibles en dehors de ce dernier

``export`` : mot clé
``import`` : **import** nomDeLaClasse **from** + le chemin d'accès c-à-d **"leNomDuFichierTS";**   
``* as`` : ``*`` tout importer, ``as`` permet d'assigner un nom ; changer de nom

- **⭕️ EXPORT**
````ts
//Module
export interface ICar{ // veut "exporter" l'interface "ICar"
    model: string;
    km: number;
}

export class Car implements ICar{
    constructor(public model: string, public km: number){}
}

//✅SIMPLIFICATION✅

//SYNTAXE 1
export {Car, ICar}; // (exporte= objet,) = classe + interface (ici) Car: classe, Icar: Intertface

//SYNTAXE 2
export default Car;
````

- **⭕️ IMPORT**

````ts
//quand la classe est exportée par défaut 
import Car from './index';

let car1 = new Car (model: "Fiat", km: 8932);

// quand module n'est pas exporter par défaut utilise : " * as "
import * as fromCar from './index'; // from "LE NOM DU FICHIER"

let car = new fromCar.Car (model: "Fiat", km: 1234); 

//récupère qu'un élement dans mon objet
import {Car} from './index';

let car2 = new Car (model: "Fiat", km: 3456);
````

- **⭕️TABLEAU IMPORT**

````ts
// quand plusieurs exportation = faire un tableau 
//EXPORT
export function carFactory(): Car[]{
    return [new Car(model: "Fiat", km: 12)];
}

export {Car as CarClass} // change de nom "Car" en "CarClass" avec le mot clé "as"

//IMPORT
import {car, ICar, carFactory as Factory} from './index'; // change de nom de "carFactory" en "Factory"

let cars = carFactory(); // va racupérer le tableau
````
---
## ⚜️TS CONFIG⚜️

---

**MANIPULATION DANS LA TERMINAL**

**OPTION** : (Ajoute l'option + le nom du fichier .ts)

- -h / tsc --help` : regarder le manuel 
- ``tsc -w / --watch`` : écouter le fichier + les modifications du fichier
- ``tsc --pretty`` : styliser les messages d'erreur avec des couleurs
- ``tsc -v / ---version`` : Afficher la version du transpiler
- ``tsc --init`` : Initialiser le ficher tsconfig.json / Paramètre
  ➡️ va créer un fichier tsconfig.json en dessous du package.json
- `tsc b / --build` : Pour construire notre projet ☞ Pour partager un projet avec les autres    
    ➡️ va créer un fichier 'build' dans le fichier .ts précisé précédemment
  