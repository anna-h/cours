![logo](./assets/mysql-logo.png)

# SOMMAIRE
  -   [Installation](#installation)     
  -    [Créer une connexion au server mysql](#crer-connexion-au-server-mysql-base-de-donne)
  -   [Créer une table (découverte des options)](#crer-une-table-dcouverte-des-options)
  -   [Configuration d'une table](#configuration-dune-table)
  -   [Insérer des données "insert into"](#insrer-des-donnes-insert-into)
  -   [Supprimer des données "delete from"](#supprimer-des-donnes-delete-from)
  -   [Mise à jour des données "update"](#mise--jour-des-donnes-update)
  -   [Sélectionner des données avec "select"](#slectionner-des-donnes-avec-select)
  -   [Trier les données avec "order by"](#trier-les-donnes-avec-order-by)
  -   [Limiter les données sélectionnées avec "limit"](#limiter-les-donnes-slectionnes-avec-limit)
  -   [Compter les données avec la fonction "count](#compter-les-donnes-avec-la-fonction-count)
  -   [Grouper les données & Compter par groupe](#grouper-les-donnes--compter-par-groupe)
  -   [Additionner les données grâce à la fonction numérique "sum"](#additionner-les-donnes-grce--la-fonction-numrique-sum)
  -   [Rechercher des données de manières partielle](#rechercher-des-donnes-de-manire-partielle)


    SQL: Structured Query Language ,C'est un système de gestion de base de données relationnelles (écrit en 1995, par Michael Widenius)
    Permet exploiter des bases de données relationnelles (rechercher, ajouter, modifier, supprimer)
    MySGL: est un système de gestion de base de données relationnelles 
    ≠ SQL: langage standard pour effectuer des requête à notre base de données relationnelles 

- Base de données relationnelles
![base de données relationnelles](./assets/base-de-donnnées-relationnelles.png)
  
        Base de données relationnelles : est une base données où l'information est organisée dans des tableaux à 2 dimensions (table / relation) 
        Ligne de relations : enregistrement / records 
        Colonne de relations : attribut / champs / colonnes

## Installation

![installation](./assets/mysql-installation.png)
![installation](./assets/mysql-installation1.png)
![installation](./assets/mysql-installation2.png)
![installation](./assets/mysql-installation3.png)
![installation](./assets/mysql-installation4.png)

## Créer connexion au server mysql (base de donnée)

`Hostname` : doit correspondre à l'adresse IP de la machine accueillante mysql ciblé    
`Port par défaut` = 3306

- **Créer une connexion**
![créer](./assets/mysql-créer-connexion1.png)
![créer connexion](./assets/mysql-créer-connexion.png)

Résultat :        
![résultat](./assets/mysql-créer-connexion-2.png)

`schema` : base de donnée
    
- **Créer un schema (base de donnée)**
![schema](./assets/my-sql-connexion3.png)
![schema](./assets/mysql-connexion-4.png)
  
## Créer une table (découverte des options)

![table](./assets/mysql-table1.png)

- Configuration ⬇️
  
- **DATATYPE**             
![datatype](./assets/my-sql-datatype.png)
  
- Dans une table → on a une clé primaire (primary key)         
  - `(PK) Primary Key` : permet d'identifier chaque enregistrement dans une table de base de donnée, ❗️ valeur doit être unique, ne peut pas de valeur **null** ❗                
  - `(NN) Not Null` : ne pas contenir de valeur **null**, donc obliger d'insert une valeur       
  - `(UQ) Unique` : permet d'indiquer cette colonne sera **unique**                 
  - `(BIN) Binary` : va avoir une donnée type **binaire** ≠ pas de texte         
  - `(UN) Unsigned` : un nombre non signé (**positif / négatif**) (par défaut le nombre va être positif)              
  - `(ZF) Zerofill` : permet des espaces           
  - `(AI) Auto Increment` : s'auto-incrémente         
  - `(G) Generated ` : permet d'indiquer à cette colonne va contenir une valeur qui va être calculé , la valeur va être **virtual(va être calculé à chaque enregistrement) / stored(va être stocké)** → dépends une **expression**           
  - `Default / Expression` : permet d'insérer une valeur par défaut

## Configuration d'une table 
![table config](./assets/mysql-table-config.png)

- **RÉSULTAT** (code sql):        
![résultat](./assets/mysql-table-code-sql.png)
  
## Insérer des données "insert into"

`INSERT INTO` : suivie du nom de la table, dans laquelle on souhaite insérer une nouvelle entrée avec `VALUES`: différents valeurs à insérer


- (valeur : 1er colonne = **id**, 2e colonne = **name**)             
![insert into](./assets/mysql-inser-into.png)
  
- Visualiser le contenu (clique droit, "**Select Rows - Limit 100**")
![insert into](./assets/my-sql-insert-into-5.png)
  
- Quand on veut supprimer une valeur => mettre **null** (si la colonne accepte 'null' ou si elle 'auto-incrémente' la meme manière que l'id)                 
![insert into](./assets/my-sql-insert-into-1.png)
  
- L'éclair : exécute les données, renvoi sans "Select Rows - Limit 100"          
![éclair](./assets/my-sql-élcair.png)
  
- re-lister
![insert-into](./assets/my-sql-modif.png)

-  Identifier les colonnes
![insert-into](./assets/my-sql-insert-into-4.png)

- **(name, created_at)** = cette syntaxe permet de sélectionner des colonnes pour nous être utile pour insérer une donnée ⬇️
![insert-into](./assets/my-sql-inser-into-6.png)
  
- Ajouter plusieurs valeurs 
![insert into](./assets/my-sql-insert-into-7.png)
  
----

## Supprimer des données "delete from" 

`DELETE FROM` : supprimer des données d'une table, suivi `WHERE` : permet de cibler des donnes en particulier 

![delete from](./assets/musql-deletefrom.png)

Cela à bien supprimé l'id position 2                    
![delete from](./assets/mysql-deletefrom-2.png)

`Safe Updates` : ce mode empêche la suppression / modification des enregistrements lorsqu'on n'utilise pas une clé primaire (PK)

- Désactiver le **Safe mode** :
  - Préférence 
  - SQL Editor
  - Décocher Safe Updates
  
- Suppression d'une donnée grâce à une condition                
![delet from](./assets/mysql-deletefrom-5.png)

Si mon price_ttc est supérieur à 50 = supprime (donc à bien supprimer l'index 4)
![delete from](./assets/mysql-delete-from5.png)

`AND` : rajouter plusieurs conditions à la suite 
![delete from](./assets/mysql-deletefrom-6.png)

---

## Mise à jour des données "update"

`UPDATE` : suivie du nom de la table pour mettre à jour des données dans une table, & `SET` : qui va nous servir à préciser la colonne à mettre à jour + la nouvelle valeur pour la colonne

- Éclair
![éclair](./assets/mysql-maj.png)
1. Enregistre juste un enregistrement en particulier (enregistre l'élément à enregistrer)
2. Sélectionne tous les enregistrements (ensuite sélectionne tout pour enregistrer)

![maj](./assets/mysql-maj-2.png)

- ⭕️ Avec une condition 
  (Met le stock à 0, si **created_at** = 2020 et la **différence** = 2)
![maj](./assets/mysql-maj-3.png)
  
---

## Sélectionner des données avec "select"

`select...from` : récupérer la listes des utilisateurs, informations d'un article, messages de conversation, tout autres informations de bases de données...., suivi de `FROM` récupérer ce qu'il a dans la table             
`*` : sélecteur qui sert à tout sélectionner

![select](./assets/mysql-select.png)
![select](./assets/mysql-select1.png)

- ⭕ Avec la condition             
![select](./assets/mysql-select-2.png) 
  
----

## Trier les données avec "order by"

    → va organiser tous les données qu'on récupère

`order by` : trier par      
`desc` : décroissant    
`asc` : ascendant ️(par défaut) 

![order by](./assets/mysql-orderby.png)

---

## Limiter les données sélectionnées avec "limit"

`limit` : utiliser pour limiter le nombre de résultats retournés

le 1er nombre corresponds au début (à partir de l'indice), le 2e nombre corresponds au nombre d'éléments que je vais récupérer :             

![limit](./assets/mysql-limit.png)

- ⭕️ Afficher plusieurs éléments au choix dans une page aléatoire, pour cela faire un calcul l'**indice doit être multiplié par le nombre d'éléments afficher - 1**
  - _Exemple_ : 
  - Afficher 10 éléments à la page 2 = **limit 10, 10**  
  - Afficher 10 éléments à la page 3 = **limit 20, 10** (page 3 = 3 * 10 (nombre d'éléments voulu) = 30, 3-1= 2(il faut soustraire), =20)


![limit](./assets/mysql-limit-2.png)

- ⭕️ Avec condition  

![limit](./assets/mysql-limit-1.png)
  
----

## Compter les données avec la fonction "count"

`count()` : va retourner le nombre d'entrées d'une colonne, suivi `WHERE` : pour qu'elle retourne le nombre d'entrées qui vont satisfaire à une certaine condition

![count](./assets/mysql-count.png)

⚠️Quand sélectionne une colonne précise, à vérifier si les éléments ne sont pas `null(NN)`= cela ne compte pas comme un nombre(élément) ⚠️    
→ il faut que le nombre / éléments doive avoir un nombre (calculable) ⛔ **null** = 0 ⛔️️ ️ 

✅ Vaut mieux compter avec l'id ou avec _*_ (l'étoile)

`as` : renommer

- Renommer **count** par **total**             
![count](./assets/mysql-count1.png)
  
---

## Grouper les données & Compter par groupe

`group by` : regroupe les données 

- Groupe les données 
![group by](./assets/mysql-group-by.png)

- Compte et groupe les données         
![group by count](./assets/mysql-group-by-count.png)
  
---

## Additionner les données grâce à la fonction numérique "sum"

`sum()` : retourne la somme des valeurs d'une colonne contenant des valeurs numériques

- Additionne           
![sum](./assets/mysql-sum.png)

- Groupe + additionne les données           
![sum](./assets/mysql-sum1.png)
  
---

## Rechercher des données de manière partielle

`LIKE "% %"`  : permet de faire une recherche partielle


![rechercher](./assets/mysql-rechercher.png)

- Recherche pour une donnée spécifique
![rechercher](./assets/mysl-rechercher-1.png)