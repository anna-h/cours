
![api-logo](assets/api-logo.png)
---
# API (Application Programming Interface)

---

# SOMMAIRE 

- [Présentation](#prsentation)
- [HTTP (HyperText Transfert Protocol)](#hypertext-transfert-protocol-http)
- [Logiciel pour tester une API](#logiciel-pour-tester-une-api-vrification)
- [Premier API(NodeJs Natif)](#premier-api-nodejs-natif)
- [Installer ExpressJs](#installation-expressjs)
- [Créer un server http avec expressJs](#crer-un-server-http-avec-expressjs)
- [Méthode HTTP(paramètre de route & contenue de la requête)](#mthode-http-paramtre-de-route--contenue-de-la-requte)
- [Changer le statut](#changer-le-statut-de-la-rponse)

---

## PRÉSENTATION

---

Application Programming Interface = Interface programmation applicative

        Fournir une interface entre 2 programmes (class, method, fonction, constante) qui va fournir un autre programme

[☞ Lien en détail sur le schéma(HOW DOES API WORK?)](https://www.business2community.com/marketing-automation/what-is-api-testing-and-how-do-you-implement-it-02358294)

**SCHEMAS**   
![api-works1](assets/api-works.png)

![api-works2](assets/api-works-2.png)

![api-works3](assets/api-works-99.png)

**STEP 1** : API REQUESTS (La requête, demande une donnée)      
**STEP 2** : DATA SOURCE SERVER (Va créer la demande => donc une réponse)      
**STEP 3** : API RESPONSE (Va renvoyer la réponse)      
**STEP 4** : APPLICATION OR CLIENT (Le client va recevoir la réponse => donc va faire son processus)

![api-works-1](assets/api-works-3.png)          
➡️ Les 3 clients vont avoir le même API et l'API est lié au même serveur (base de donnée)

---

## HYPERTEXT TRANSFERT PROTOCOL (HTTP)

---

    Est conçu pour communiquer un navigateur + serveur web

Client = le navigateur web va envoyer une requête http → va ouvrir une connection entre le navigateur web + le serveur → donc le serveur va renvoyer une donnée/ réponse = cette connection http va être coupé = **protocol sans état**      

 [☞ Lien "Méthodes de requête HTTP"(mdn/ liste + description)](https://developer.mozilla.org/fr/docs/Web/HTTP/Méthode)

![http](assets/api-http-1.png)
![http](assets/api-http.png)

Nous pouvons aussi avoir de la clean architecture qui lie avec la méthode http :      
![crud-http](assets/api-crud-http.png)

⚠️**Request URL** :https:// ,**Request Method** : GET (fait référence au tableau⬆️) **obligation d'en avoir 1**⚠️

**⭕️LISTE DE CODES HTTP** (statut code): [☞Liste de tous les codes HTTP](https://fr.wikipedia.org/wiki/Liste_des_codes_HTTP)    
![api-liste-de-codes-http](assets/api-liste-de-codes-http.png)




---

## LOGICIEL POUR TESTER UNE API (vérification)

--- 

    A 2 apllications pour tester l'API = Insomnia, Postman

### INSOMNIA

[☞Lien utilisé (api.gouv.fr) + description des méthodes utilisées](https://geo.api.gouv.fr/adresse)

![insomnia](assets/api-insomnia.png)

![core-params](assets/api-core-params.png)

![insomnia-test](assets/api-insomnia-test.png)

`&limit` = le nombre de donnés (qu'on donne dans l'URL)

![insomnia-test](assets/api-inomnia-test-1.png)

Peut avoir des **API public** & **API privé**
- **API public** = tout le monde peut avoir accès
-  **API privé** = faut faire une demande pour avoir une clé d'accès **token**→ à chaque fois qu'on veut y avoir accès il faut avoir cette clé pour rentrer

---

## PREMIER API (NodeJs NATIF) 

---

## INSTALLATION EXPRESS.Js

---
![expressjs-logo](assets/expressjs-logo.png)

       EXPRESS = Infrastructure Web minimaliste, souple et rapide pour node.js

Elle apporte une **couche fine de fonctionnalités** qui permet de **développer des infrastructures web en nodeJs** tout en gardant des **modules nodeJs** 


![express-installation](assets/express-site.png)

`cd..` : les deux petits points = (déplacement) retour au dossier parent (comme dans le HTML)

---

## CRÉER UN SERVER HTTP AVEC EXPRESSJs

---

- JS
`````javascript
//fichier index.js
const express = require('express.api'); //require = récupérer

//créer un serveur

const server = express();

//ajouter une route
server.get('/', (request, response) =>{//renvoie un message dans le site

    response.send('Hello World !'); //récupère la réponse
});

server.listen(8080, () =>{ //activer le server ".listen", écoute le port "8080"
    console.log('Server running on http:://localhost:8080');
});
`````

**RÉSULTAT**      
![express](assets/api-express.png)

---

##  MÉTHODE HTTP (paramètre de route & contenue de la requête)

--- 

`request.params.+lenomduparamètre` = récupérer un paramètre dans l'URL
`data` = les données de mon paramètre

````javascript
//fichier index.js
const express = require('express');

const server = express();

// ❗️IMPORTANT❗️ récupère le contenu de la requête
server.use(express.json()); //Permet de parser "json" de insomnia => donc les body //sert à visionner le "body"


// route / , méthode GET
server.get('/',(request, response) => {
    response.send('Mon premier serveur API');
});


// route /users méthode ⭕GET
server.get('/users', (request, response) =>{
    const users = [];
    response.send({users});
});

//route /users methode ⭕ POST
server.post('/users', (request, response) =>{
    console.log(request.body.user);

    response.send({message: 'Users saved', user: 'Sam', request: request.body.user}); //envoie une donnée, avec une clé "request" = ma valeur d'utilisateur envoyé dans body
});


// route /users/:id methode ⭕PUT
server.put('/users/:id', (request, response) =>{

    response.send({message: 'User updated successfully!', user: request.params.id, data: request.body})
})

//  route /users/:id méthode ⭕DELETE // id = paramètre
server.delete('/users/:id', (request,response) => { // après ":" peut être modifié

    console.log(request.params);

    response.send({message: 'User successfully deleted!', user: request.params.id})
});


server.listen(8080, () =>{
    console.log('Server running on http://localhost:8080');
});
````

**⭕️GET**
(quand on écrit dans l'application "insomnia")      
![express-get](assets/express-get.png)
(va afficher dans la terminal)           
![express-get](assets/express-get1.png)


**⭕️ DELETE**
![express-delete](assets/experess-delete.png)

**⭕️ PUT**
![express-put](assets/express-put.png)

---

## CHANGER LE STATUT DE LA RÉPONSE


statut= code HTTP

**⭕️ MODIFIER LE STATUT**
- JS (delete)
````javascript
//fichier index.js

//  route /users/:id méthode ⭕DELETE // id = paramètre
server.delete('/users/:id', (request,response) => { // après ":" peut être modifié

    //↪️ vérifie que l'id corresponds à un utilisateur dans la base de données
    const userExist = false;
    if (userExist === false){
        response.status(404).send('User not found: ' + request.params.id)
    }
    console.log(request.params);
//SYNTAXE 1 ⏺
    response.status(201).send({message: 'User successfully deleted!', user: request.params.id}) //change le code HTTP
});

````

![modif-statut](assets/api-statut-1.png)
![modif-statut-if](assets/api-statut-1-if.png)

- JS (post)
```javascript
//route /users methode ⭕️POST
server.post('/users', (request, response) =>{
    console.log(request.body.user);
    // faire des vérifications
    if (request.body.email){

    }else {

    }

    // logique

    //SYNTAXE 2⏺
    response.statusCode = 404;

    response.send({message: 'Users saved', user: 'Sam', request: request.body.user}); //envoie une donnée, avec une clé "request" = ma valeur d'utilisateur envoyé dans body
});
```

![modif-statut](assets/api-statut-2.png)