![react-logo](../assets/react-logo.png)

---

## SOMMAIRE

- [Premier élément](#premier-element)
- [Syntaxe JSX](#syntaxe-jsx)
- [Déclarer un composant](#dclarer-un-composant)
- [Component Lifecycle](#component-lifecycle)
- [Component State](#component-state)
- [Gérer les évènements](#grer-les-vnements)
- [Affichage conditionnel](#affichage-conditionnel)
- [Lists key](#lists-key)
- [Create React App](#create-react-app)
- [Formulaire](#formulaire)
- [Router](#router)

---








---
# REACT JS 

---

    C'est une librairie Javascript qui permet de créer des "single page application", pour leur interface utilisateurs
    Créer par Facebook pour construire des interfaces web, maintenant très utiliser par plusieurs grandes entreprises (Twitter, Netflix, Airbnb Chrome ...)
    Lorsque les données changeront, React mettra à jour, de façon optimale juste les composants qui en auront besoin 

---

## PREMIER ELEMENT

---

**INSTALLER REACT** :   
*ÉTAPE 1* : Aller sur leur site officiel ☞ [React](https://reactjs.org/)  
*ÉTAPE 2* : Aller sur la page "Docs" puis "Liens CDN"   
*ÉTAPE 3* : Copier les 2 liens CDN          
![react](../assets/react-lien-cdn.png)

---
**CRÉER UN PREMIER COMPOSANT** :   

- HTML
````html
<!doctype html>
<html lang="fr">
<head>
<meta charset="UTF-8">
             <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
                         <meta http-equiv="X-UA-Compatible" content="ie=edge">
             <title>React js</title>
</head>
<body>

<div id="app"></div>

<script crossorigin src="https://unpkg.com/react@17/umd/react.development.js"></script>
<script crossorigin src="https://unpkg.com/react-dom@17/umd/react-dom.development.js"></script>
<sript scr="app.js"></sript>
</body>
</html>
````

- JS
````javascript
// fichier app.js

const title = React.createElement('h1', {}, 'Hello world'); // dans le DOM
// 1er argu : "tageName" = h1
// 2e argu : "option" = {}
// à partir du 3e argu : "children" = hello world 

ReactDOM.render(title, document.querySelector('#app')); // va selectionner la div dans le HTML 

console.log(title);
````

- **1er argument : "tageName"** = h1
- **2e argument : "option"** = {}
- **à partir du 3e argument : "children"** = Hello world 

`ReactDOM.render` = méthode, faire le rendu d'un / des élément(s) React dans DOM    
`render` = rend du HTML dans l'élément

- JS 
````javascript
// fichier app.js
// va prendre le rendu dynamique (avec les secondes)

let n = 1;
function render (){
    const title = React.createElement('h1', {},
        'Secondes :',
        React.createElement('span', {}, n)
    );

    ReactDOM.render(title, document.querySelector('#app'));
}

render();

setInterval(() =>{
    n++;
    render();
}, 1000);
````

- JSX (simplification de js ≠ donne le même résultat)
````javascript
class Timer extends React.Component {
  constructor(props) {
    super(props);
    this.state = { seconds: 0 };
  }

  tick() {
    this.setState(state => ({
      seconds: state.seconds + 1
    }));
  }

  componentDidMount() {
    this.interval = setInterval(() => this.tick(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    return (
      <div>
        Seconds: {this.state.seconds}
      </div>
    );
  }
}

ReactDOM.render(
  <Timer />,
  document.getElementById('timer-example')
);
````
 **RÉSULTAT** :   
![premier élément](../assets/react-premier-element.png)

---
## SYNTAXE JSX

---

**JSX** = c'est les équipes de React qui ont créé, syntaxe basée sur Javascript, qui permet de **mélanger HTML & JS**    
**babel** = est compilateur Javascript, qui va convertir du **JSX** en **JS Native**

- **INSTALLER JSX** :        
![jsx](../assets/react-jsx.png)
  
- HTML
````html
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>React js</title>
</head>
<body>

<div id="app"></div>

<script crossorigin src="https://unpkg.com/react@17/umd/react.development.js"></script>
<script crossorigin src="https://unpkg.com/react-dom@17/umd/react-dom.development.js"></script>
<script src="https://unpkg.com/babel-standalone@6/babel.min.js"></script>


<script src="app.jsx" type="text/babel"></script>
</body>
</html>
````

Le fichier "app.jsx" est du JSX qui va être interprété par babel, (donc traduire le jsx par le compilateur babel en js native)

- JSX
````javascript
let n = 1;
function render (){
    const title = <h1>Secondes : <span>{n}</span></h1>

    ReactDOM.render(title, document.querySelector('#app'));
}

render();

setInterval(() =>{
    n++;
    render();
}, 1000);
````

Pas d'espace entre les 'children' avec saut de ligne ≠ faire un espace avec le clavier + écrire tout sur la même ligne   
≠ ou sinon saut de ligne + { ' ' }

- JSX
```javascript
    const title = <h1>Secondes : <span>{['Anna', 'Bibi']} {1 + 4}</span> </h1>
// va automatiquement faire une concaténation 
```

**RÉSULTAT** :           
![jsx](../assets/react-jsx-1.png)


❌NE PAS FAIRE ÇA❌ `<span>{{name: 'Anna'}}</span>` **ERROR** (non un objet)      
✅À FAIRE✅ `<span>{{name: 'Anna'}.name}</span>` (peut afficher la propriété d'un objet)

---
⭕️ **CRÉER UN TABLEAU**

- JSX
    
````javascript
let n = 1;

function render (){

    let tags = [
        'Tag 1',
        'Tag 2',
        'Tag 3'
    ];


    const title = <div>
        <h1>Secondes : <span>{n}</span></h1>
        <ul>
            <li>Tag 1</li>
            <li>Tag 2</li>
            <li>Tag 3</li>
        </ul>
    </div>

    ReactDOM.render(title, document.querySelector('#app'));
}
````

Dans **JSX** à droit de mettre qu'**une seule racine de HTML** donc pour mettre plusieurs propriétés dans le JSX il faut tout mette dans une **div** ⬆️   
☞ Pour ne pas afficher **div** dans la console à ne pas écrire div mais **<> </>** = fragment (à les laisser vide pour ne pas avoir la notation div)   
→ ou sinon **<React.Fragment></React.Fragment>** 

stocker dans un tableau  d'éléments 

- JS
````javascript

function render (){

    let tags = [
        'Tag 1',
        'Tag 2',
        'Tag 3'
    ];

    // va afficher "tags" sans écrire des "li" à chaque fois  => tout est stocker dans ce tableau d'éléments
    const tagsEl = tags.map ((tag, index) => {
        // en callback va passer une fonction fléchée
        // return li
        return <li key={index}>{tag}</li> // "key" = fragment
    })


    const title = <div>
        <h1 id={"title-"+n}>Secondes : <span>{n}</span></h1> // va être en dynamique 
        <ul>{tagsEl}</ul>
    </div>

    ReactDOM.render(title, document.querySelector('#app'));
}
````

➡️ La syntaxe `<React.Fragment>` peuvent avoir des **clées** ,En cas d'utilisation consisterait à faire correspondre une collection à un tableau de fragment    
**key** = est le seul attribut qui peut être passé à **fragment**    
`id={" "}` = en mettant des **accolades** permet de faire du **DOM (dynamique)**

❗️ Mettre du js et non du html❗️  

❌NE PAS FAIRE❌ `class=" "`   
✅À FAIRE✅ `className=" "`

**RÉSULTAT** ("id" en dynamique) :       
![id-dynamique](../assets/react-jsx-2.png)

---

## DÉCLARER UN COMPOSANT

---

![react-composant](../assets/react-composant.png)

- JSX (SYNTAXE 1)
```javascript
function Compteur(props){ //props = propriétés
    return <div>
        <h1>Compteur : <span>{props.name}</span></h1>
        <p>{props.children}</p>
    </div>
}

ReactDOM.render(<Compteur name="Anna">Anna est gentille</Compteur>, document.querySelector('#app'));
```

Quand on ajoute une donnée entre une balise d'un composant → direct la donnée devient un 'children'

- JSX (SYNTAXE 2)
```javascript
function CompteurFn(props){ //props = propriétés
    console.log(props);
    return <div>
        <h1>Compteur : <span>{props.name}</span></h1>
        <p>{props.children}</p>
    </div>
}

//Affiche 
class Compteur extends React.Component{

    render(){
        // Pour afficher juste le "name"
        // A ne pas oublier 'this'
        console.log(this.props);
        return <h1>{this.props.name}</h1>;
    }
}

ReactDOM.render(<Compteur name="Anna">Anna est gentille</Compteur>, document.querySelector('#app'));
```

**AFFICHER 2 COMPOSANTS** (autre syntaxe)

- JSX
````javascript
class Accueil extends React.component{

    render(){
        return <div>
            <CompteurFn name={'Anna'}/> // 1er composant
            <Compteur name={'Tony'}>Il s'appelle Tony</Compteur> // 2e composant
        </div>
    }
}

ReactDOM.render(<Accueil/>, document.querySelector('#app'));
````


---

## COMPONENT LIFECYCLE

----

![component-lifecycle](../assets/react-component-lifecycle.png)

**MONTAGE**

    Les méthodes suisantes sont appelées dans cet ordre lorsqu'une instace d'un composant créée + insérée dans le DOM
1. `constructor()`
2. `static getDerivedStateFromProps`
3. `render()`
4. `componentDidMount()`

- JSX
```javascript
function CompteurFn(props){ //props = propriétés
    console.log(props);
    return <div>
        <h1>Compteur : <span>{props.name}</span></h1>
        <p>{props.children}</p>
    </div>
}

class Compteur extends React.Component{

    //⭕️constuctor()
    constructor(props) {
        super(props);
        console.log('1');
        this.state = {ok: 1};
    }

    //⭕️static getDerivedStateFromProps
    static getDerivedStateFromProps(state){
        console.log('2');
        return state;
    }

    //⭕️render
    render(){
        console.log('3');
        return <div>
            <h1>Compteur: <span>{this.props.name}</span></h1>
            <p>{this.props.children}</p>
        </div>;
    }

    //⭕️componentDidMount
    componentDidMount(){
        console.log('4');
    }
}

ReactDOM.render(<Compteur/>, document.querySelector('#app'));
```


⭕️ **MISE À JOUR**

    Permet de déclencher par des changements dans les props / l'état local

1. `static getDerivedStateFromProps()`
2. `shouldComponentUpdate()`
3. `render()`
4. `getSnapshotBeforeUpdate()`
5. `componentDiUpdate()`

- JSX
````javascript
    //⭕️componentDidUpdate
    componentDidUpdate(){
        console.log('5'); // ne s'affiche pas, car il n'y a eu aucune modification
    }
````

⭕️**DÉMONTAGE**

    La méthode suivante est appelée quand un composant est retiré du DOM

- `componentWillUnmont()`  


- JSX 
````javascript
    //⭕️componentWillUnmont
    componentWillUnmont(){
        console.log('6'); // démontage
    }
````

**RÉSULTAT** ('5' & '6' ne s'affiche pas)         
![component-lifecycle-1](../assets/react-component-lifecycle-1.png)

---

## COMPONENT STATE

---

`state` = état local

    Un composant a besoin d'un 'state' quand des données vont évoluer dans le temps 
_Exemple_ : `checkbox` → `isChecked`, `NewsFeed` → `fetchedPosts` (composant → état local)

☞ **Différence entre `state` & `props`**

`props` = passe depuis le composant parent     
`state` = géré en interne par le composant en lui-même   
→ Un composant **ne peut pas changer** ses `props` ≠ mais **peut changer** son `state`

- JSX
```javascript
function CompteurFn(props){ //props = propriétés
    console.log(props);
    return <div>
        <h1>Compteur : <span>{props.name}</span></h1>
        <p>{props.children}</p>
    </div>
}

class Compteur extends React.Component{

    constructor(props) {
        super(props);

        this.state = { // initialiser de l'état local avec 'this.'
            //valeur initiale
            name: props.name, //les propriétés de name
            nb : 0,
            
        }



        setInterval(() =>{
            this.setState({
                nb: this.state.nb +1,
            });
        },1000);
    }

    render(){

        return <div>
            <h1>{this.state.name}</h1>
            <p>{this.state.nb}</p>
        </div>;
    }
}

class Accueil extends React.Component{

    render() {
        return (
            <div>
                <CompteurFn name={"Anna"}></CompteurFn>
                <Compteur name={"Anthony"} >Il s'appelle Anthony</Compteur>
            </div>
        );
    }
}

ReactDOM.render(<Accueil/>, document.querySelector('#app'));
```

`this.state` = initiale l'état local   
`this.setState()` = modifier le state, écrase les modifications qu'on a portées    
❌INTERDIT DE MODIFIER AVEC `this.state` ❌

![state-component](../assets/react-component-state-1.png)

⭕️**VALEUR PAR DÉFAUT**

- JSX
````javascript
function CompteurFn(props){ //props = propriétés
    console.log(props);
    return <div>
        <h1>Compteur : <span>{props.name}</span></h1>
        <p>{props.children}</p>
    </div>
}

class Compteur extends React.Component{

    constructor(props) {
        super(props);

        this.state = { // initialiser de l'état local avec 'this.'
            //valeur initiale
            name: props.name, //les propriétés de name
            nb : props.defaultValue,
        }



        setInterval(() =>{
            this.setState({
                nb: this.state.nb +1,
            });
        },1000);
    }

    render(){

        return <div>
            <h1>{this.state.name}</h1>
            <p>{this.state.nb}</p>
        </div>;
    }
}

class Accueil extends React.Component{

    render() {
        return (
            <div>

                <CompteurFn name={"Anna"}></CompteurFn>
                <Compteur name={"Anthony"} defaultValue={89}>Il s'appelle Anthony</Compteur>
                <Compteur name={"Anthony"} defaultValue={73}>Il s'appelle Anthony</Compteur>
                <Compteur name={"Anthony"} defaultValue={99}>Il s'appelle Anthony</Compteur>
            </div>
        );
    }
}

ReactDOM.render(<Accueil/>, document.querySelector('#app'));
````

**RÉSULTAT** (ne commence pas en même temps, comme défini)        
![composant-state](../assets/react-component-state.png)

----

## GÉRER LES ÉVÈNEMENTS

---

→ Les évènements de React sont nommés en **camelCase**   
→ En JSX on passe une fonction comme gestionnaire d'évènements plutôt qu'une chaîne de caractères


- JSX (⭕️SYNTAXE 1(fonction))
```javascript
function CompteurFn(props){ //props = propriétés
    console.log(props);
    return <div>
        <h1>Compteur : <span>{props.name}</span></h1>
        <p>{props.children}</p>
    </div>
}

class Compteur extends React.Component{

    constructor(props) {
        super(props);

        this.state = { // initialiser de l'état local avec 'this.'
            //valeur initiale
            name: props.name, //les propriétés de name
            nb : props.defaultValue,
        }

    }

    increment(){
        console.log(this);
        console.log('increment', this.state.nb);
        //a chaque fois que j'incrémente ajoute +1
        this.setState((state) =>{ //va passer une fonction fléchée
            // en mettant 'state' en paramètre ≠ pas besoin de mettre 'this' dans return 
            return{
                nb: state.nb + 1 //passer directement pas la variable 'state'
            }
        })

    }
    render(){

        //⭕️SYNTAXE 1
        return <div>
            <h1>{this.state.name}</h1>
            <p>{this.state.nb}</p>

            <button onClick={this.increment.bind(this)}>Incrémente</button>
        </div>;
        //donne la référence et non la fonction
        //'bind' pour lier des fonctions
    }
}

class Accueil extends React.Component{

    render() {
        return (
            <div>

                <Compteur name={"Anna"} defaultValue={99}></Compteur>
            </div>
        );
    }
}

ReactDOM.render(<Accueil/>, document.querySelector('#app'));
```

`bind`= lier les fonctions de rappel 

**RÉSULTAT** :
![gerer-les-evenements](../assets/react-gerer-les-evenements.png)

⭕️SYNTAXE 2 (fonction)

- JSX
````javascript

function CompteurFn(props){ //props = propriétés
    console.log(props);
    return <div>
        <h1>Compteur : <span>{props.name}</span></h1>
        <p>{props.children}</p>
    </div>
}

class Compteur extends React.Component{

    constructor(props) {
        super(props);

        this.state = { // initialiser de l'état local avec 'this.'
            //valeur initiale
            name: props.name, //les propriétés de name
            nb : props.defaultValue,
        }

    }

    increment(){
        console.log(this);
        console.log('increment', this.state.nb);
        //a chaque fois que j'incrémente ajoute +1
        this.setState((state) =>{ //va passer une fonction fléchée
            // en mettant 'state' en paramètre ≠ pas besoin de mettre 'this' dans return
            return{
                nb: state.nb + 1 //passer directement pas la variable 'state'
            }
        })

    }
    render(){

        //⭕️SYNTAXE 2
        return <div>
            <h1>{this.state.name}</h1>
            <p>{this.state.nb}</p>

            <button onClick={() => this.increment()}>Incrémente</button>
        </div>;
        // lui donne une fonction fléchée
        // 'this.increment' va être lier dans la fonction, grâce à la fonction fléchée
        //'this' sera référence au 'this' parent

    }
}

class Accueil extends React.Component{
    
    render() {
        return (
            <div>

                <Compteur name={"Anna"} defaultValue={99}></Compteur>
            </div>
        );
    }
}

ReactDOM.render(<Accueil/>, document.querySelector('#app'));
````
---

⭕️ **PASSER DES ARGUMENTS À UN GESTIONNAIRE D'ÉVÈNEMENTS**

Au sein d'une boucle, passer un argument supplémentaire à un gestionnaire d'évènements

- JSX
````javascript
increment(id){
    console.log(id);
    this.setState((state) =>{
        return{
            nb: state.nb + 1
        };
    })
}

render(){
    return <div>
        <h1>{this.props.name}</h1>
        <p>{this.state.nb}</p>
        
        <button onclick={this.increment.bind(this)>Incrémente</button>
            //donner la référence à la fonction + donne 'this' pour lier la fonction dans le composant 
}
````

_Exemple_ : 

````javascript
<button onClick={(e) => this.deleteRow(id, e)}>Supprimer la ligne</button>
<button onClick={this.deleteRow.bind(this, id)}>Supprimer la ligne</button>
````

`e` = représente l'**évènement** React

L'évènement sera passé en second argument après l'ID  
→ avec la fonction fléchée => passe l'argument explicitement, ➖ alors que `bind` tous les arguments sont automatiquement transmis

---

## AFFICHAGE CONDITIONNEL

---

    Utilise l'instruction Javascript 'if' / 'opérateur ternaire' pour créer des éléments représentant l'état courant, → laisse React mettre à jour l'interface utilisateur pour qu'elle corresponde 


- JSX 
```javascript
//bouton Réinitialiser

function CompteurFn(props){ //props = propriétés
    console.log(props);
    return <div>
        <h1>Compteur : <span>{props.name}</span></h1>
        <p>{props.children}</p>
    </div>;
}

class Compteur extends React.Component{

    constructor(props) {
        super(props);

        this.state = {nb : props.defaultValue};

    }

    increment(){

        //exécute la méthode 'setState' avec la fonction arrow en paramètre 'state' → qui retourne un objet {clé nb, qui incrémenter de +1}
        this.setState((state) =>({nb: state.nb + 1 })); //retourne l'objet directement

    }

    reset(){
        this.setState(() => { //va mettre à jour le state → met en paramètre une fonction callback + retourner un objet
            return{nb : 0}; // réinitialiser à 0
        })
    }

    render(){

        //quand j'appuie le bouton 'reset' → nombre remet à 0 , donc la condition est respectée
        // sinon pas respecté le bouton 'reset' reste 'undefined'
        let btnReset; // bouton qui ne contient rien

        if (this.state.nb > 0){
            btnReset = <button onClick={() => this.reset()}>Réinitialiser</button>
        }

        return <div>
            <h1>{this.state.name}</h1>
            <p>{this.state.nb}</p>

            <button onClick={() => this.increment()}>Incrémente</button>

            {btnReset}

        </div>;
        // bouton = btn, réinitialiser → nombre ≥ 0
        //affiche le bouton {}

    }
}

class Accueil extends React.Component{

    render() {
        return (
            <div>

                <Compteur name={"Anna"} defaultValue={99}></Compteur>
            </div>
        );
    }
}

ReactDOM.render(<Accueil/>, document.querySelector('#app'));
```

Peut stocker des éléments dans une variable + afficher / les assigner dans des manières conditionnelles

**RÉSULTAT** :      
![affichage-conditionnel](../assets/react-affichage-conditionnel.png)

- JSX
```javascript
// bouton décrémenter


function CompteurFn(props){ //props = propriétés
    console.log(props);
    return <div>
        <h1>Compteur : <span>{props.name}</span></h1>
        <p>{props.children}</p>
    </div>;
}

class Compteur extends React.Component{

    constructor(props) {
        super(props);

        this.state = {nb : props.defaultValue};

    }

    increment(){

        //exécute la méthode 'setState' avec la fonction arrow en paramètre 'state' → qui retourne un objet {clé nb, qui incrémenter de +1}
        this.setState((state) =>({nb: state.nb + 1 })); //retourne l'objet directement

    }

    reset(){
        this.setState(() => { //va mettre à jour le state → met en paramètre une fonction callback + retourner un objet
            return{nb : 0}; // réinitialiser à 0
        })
    }

    //⭕️SYNTAXE 1 (opérateur ternaire)
    decrement(){
        this.setState((state) =>{ //va mettre à jour
            return{
                //moins 1 à chaque fois appuie le bouton
                nb: state.nb >= 1 ? state.nb - 1 : 0 // opérateur ternaire → si 'nb' est supérieur ou égale à 1 alors -1 sinon revoie 0, ':'  = sinon
            }
        })
    }

    render(){

        //quand j'appuie le bouton 'reset' → nombre remet à 0 , donc la condition est respectée
        // sinon pas respecté le bouton 'reset' reste 'undefined'
        let btnReset; // bouton qui ne contient rien

        if (this.state.nb > 0){
            btnReset = <button onClick={() => this.reset()}>Réinitialise</button>
        }

        return <div>
            <h1>{this.state.name}</h1>
            <p>{this.state.nb}</p>

            <button onClick={()=> this.decrement()}>Décrémente</button>

            <button onClick={() => this.increment()}>Incrémente</button>

            {btnReset}

        </div>;
        // bouton = btn, réinitialiser → nombre ≥ 0
        //affiche le bouton {}

    }
}

class Accueil extends React.Component{

    render() {
        return (
            <div>

                <Compteur name={"Anna"} defaultValue={99}></Compteur>
            </div>
        );
    }
}

ReactDOM.render(<Accueil/>, document.querySelector('#app'));
```

En appliquant l'**opérateur ternaire** c'est plus court alors que '**if**' ce fait en 4 lignes = les 2 méthodes sont valables

```javascript
//⭕️ SYNTAXE 2  (if)
decrement(){
    this.setState((state) =>{//va mettre à jour

        if (state.nb >= 1){
            return {nb: state.nb - 1};
        }
        return 0;

    })
}
```

**RÉSULTAT**      
![affichage-conditionnel](../assets/react-affichage-conditionnel-1.png)

⭕️ **CONDITION À LA VOLÉE**

        Aussi appeler l'opérateur logique Javascript &&, Permet d'inclure conditionnellement un élément , en l'enveloppant dans des accolades

❗️C'est du javascript ❗️   
`true && expression`  est toujours évalué à l'**expression**     
`false && expression` est toujours évalué **false**  
➡️Si la condition est **true** l'élément juste après **&&** sera affiché ≠ **false**, React va l'ignorer + le sauter = donc va afficher false


- JSX
```javascript

        return <div>
            <h1>{this.state.name}</h1>
            <p>{this.state.nb}</p>

            
            // ⭕️ condition à la volée
            {(this.state.nb >= 1 && <button onClick={() => this.decrement()}>Décrémente</button>)}
            

            <button onClick={() => this.increment()}>Incrémente</button>

            {btnReset}

        </div>;
```

Si cette condition en Js est vrai → va rendre ce qu'il est entre les && = ressemble au ternaire

---

## LISTS KEY

---


    Les clés aident React à identifier quels éléments d'une liste à changer / ajouter / supprimer 

➡️ Utilise `id` de notre donnée comme clé → identifie de façon unique un élément d'une liste parmi les autres    
➡️ Quand ❌ de `id` stable pour les éléments affichés❌  → utilise l'index de l'élément    
`Utilise l'index comme clé si l'ordre des éléments est susceptible de changer`    
≠ Si ne donne pas explicitement de clé aux éléments d'une liste → React utilisera l'**index** par défaut


`key` = mot clé   
React prends en charge l'attribut `key` → quand les enfants ont cette clé, React utilise cette clé pour **correspondre les enfants à l'arbre d'origine** avec **les enfants de l'arbre suivant**     

⭕️**INTÉGRER `map()` DANS DU JSX**

    Prendre un tableau de nombre + doubler leurs valeurs 
    Permet d'intégrer des expressions quelconques entre accolades → utilise map()

````javascript
class Item  extends React.Component {
	state = {
    text: this.props.text
  }
  
  onChange = event => {
    this.setState({
      text: event.target.value
    })
  }

  render() {
    const { text } = this.state;
    return (
      <li>
        <input value={text} onChange={this.onChange} />
      </li>
    )
  }
}

class App extends React.Component {
  state = {
    items: [
      {
        text: "First",
        id: 1
      },
      {
        text: "Second",
        id: 2
      }
    ]
  };

  addItem = () => {
  	// Here we push a new "Front" item at the end of the list, 
    //    but instead react duplicate the last element at the end of the list
    const items = [{ text: "Front", id: Date.now() }, ...this.state.items];
    this.setState({ items });
  };

  render() {
    console.log("Notre state", this.state);
    return (
      <div>
        <ul>
          {this.state.items.map((item, index) => (
          	// Try by setting key={item.id} here ?
            <Item {...item} key={index} />
          ))}
        </ul>
        <button onClick={this.addItem}>Add Item</button>
      </div>
    );
  }
}

ReactDOM.render(
  <App />,
  document.getElementById('container')
);
````    

**RÉSULTAT**   
![lists-key](../assets/react-lists-keys.png)

☞ [Les travers des keys en React (exemple inside)](https://www.atrakeur.com/blog/web/les-travers-des-react-keys/)

---
## FAIRE REMONTER LA DONNÉE

---

        Plusieurs composant ont souvent besoin de refléter les mêmes données dynamiques
        →Faire remonter l'état partagé dans leur parent (passage de donnée entre les composants enfant vers les composant parents)

----

## CREATE REACT APP

---

☞ [lien Create React App + Démo + explication (officiel)](https://create-react-app.dev/docs/getting-started)

![create-react-app](../assets/react-create-react-app.png)

![create-react-app-assets](../assets/react-create-react-app-assets.png)

Dans fichier "package.json" :   
`npm run` + "script" (**start**, **build**, **test**, **eject**) dans la terminal → va afficher un nouveau onglet + rajoute un **dossier** avec le nom du script 

**RÉSULTAT** (va afficher un nouveau onglet + cette page react)               
![create-my-app](../assets/react-create-react-app-1.png)

Copier le contenu **app.jsx**(dossier first-app) dans **app.js** (dossier my-app) (copier que les class + ce qu'il a dedans) → va afficher la page comme avant (☞exemple)

- JS
````javascript
// fichier app.js
import logo from './logo.svg';
import './App.css';

//syntaxe 1
import * as React from 'react'; //importer tout 'react' + insère la variable 'React'
//syntaxe 2
import {Component} from 'react'; //selectionner dans la librairie 'react' Component 
//si passe par cette synatxe enlève 'React.' car on passe direcement par le composant + React n'existe pas car n'a pas assigner la-dedans
//➡️ class Counter extends Component {...}

import Accueil from './accueil/Accueil'; //nom du dossier + nom du fichier 

class Counter extends React.Component {...}

//si ce composant à été déplacer → doit l'import ici (pour être visible)
//class Accueil extends React.Component {...}

function App() {
    return(
        <Accueil/>
    );
}

export default App;
````

**SÉPARER PLUSIEURS COMPOSANTS DANS DIFFÉRENT FICHIER**

Créer un dossier _Accueil_ → fichier _Accueil.js_

- JS
````javascript
// fichier accueil.js
import {Component} from 'react';

class Accueil extends Component {...} //copie la class

export default Accueil;
````

----

## FORMULAIRE

---

      
**LIER UNE VALEUR ET DE RÉCUPÉRER LES CHANGEMENTS D'UNE FONCTION DANS LAQUELLE ON VA METTRE À JOUR LA VALEUR**
````javascript
// fichier form.js
import ...

class Form extends Component {
    
    //lui donner une valeur 
    constructor(props){
        super(props);
        this.state = {
            name: 'Jean'
            checked: false,
        };
    }
    
    handleChange(event){
        console.log(event.target.value); //voit le changement dans la console
        this.setState({name: event.target.value}); //peut écrire dans l'input sans l'input contolé (input controlé = a 'name' + la modification)
    }
    
    handleChanheCheckedbox(event){
        console.log(event.target.value);
        this.setState({
            checked: event.target.checked //est ce que la box a été checked
        });
    }
    
    
    render () {
        
        return <div>
            <h1>Formulaire</h1>
            
            <label htmlFor="name">Nom du compteur</label>
            //⭕️SYNTAXE 1
            <input id="name" name="name" value={this.state.name} onChange={this.handleChange.bind(this)}/>
            //⭕️SYNTAXE 2
            <textarea value={this.state.name} onChange={this.handleChange.bind(this)}></textarea>
            //chekbox (petit case à coté de l'input)
            <input type="checkbox" checked={this.state.checked} onChange={this.handleChange.bind(this)}/>
            
        </div>
    }
}
````

Doit toujours fermer les composants avec les balises fermantes _ex_ :`<input/>` ou `<input></label>` ou `<input></div>` 

**RÉSULTAT** (mais ne pas être modifié directement dans l'input)       
![formulaire](../assets/react-formulaire.png)

(ce qu'affiche la console, quand on écrit dans l'input)      
![formulaire-console](../assets/react-formulaire-1.png)
`SyntheticBaseEvent` = instances, un enrobage compatible tous navigateurs autour de l'évènement natif du navigateur, fournit la même interface que l'évènement    
→ les objets **SyntheticEvent** sont recyclés + sera réutilisé & ses propriétés seront remises à **null** une fois que la fonction de rappel de l'évènement aura été invoquée 


![checkedbox](../assets/react-formulaire-2.png)

✅SIMPLIFICATION✅
````javascript
import ...

class Form extends Component {
    
    constructor (props){
        super(props);
        this.state = {
            name: 'Jean'
            checked: false,
        };
    }
    
    handleChange(event){
        const name = event.target.name;
        console.log(event.target.value);
        
        this.setState({
            [name]: event.target.value //les accolades lie avec la constante → fait la mise à jour quand on change de mot 
        });
    }
    
    render(){
        return <div>
            <h1>Formulaire</h1>
            <label htmlFor="name">Nom du compteur</label>
            <input type="text" id="name" name="name" value={this.state.name} onChange={this.handleChange.bind(this)}/>
            <br/>
            
            <label htmlFor="text">Nom du compteur</label>
            <textarea id="text" name="text" value={this.state.text} onChange={this.handleChange.bind(this)}></textarea>
            
            <br/>
            <label htmlfor="check">Checkbox</label>
            <input id="check" name="checked" type="checkbox" checked={this.state.checked} onChange={this.handleChange.bind(this)}/>
    }
    
}
````

----

## ⚜️️ROUTER⚜️

---
- **INSTALLATION**
![router](../assets/react-router.png)
![router](../assets/react-router1.png)

⏺Rajouter un système de router → cela permet de départager 2 composants (1 composant de 2 => 2 composants de 1)

````javascript
//fichier App.js
import './App.css';

import Form from './Form/Form';
import Counters from './Counters/Counters';
import Dashboard from './Dashboard/Dashboard'

//mettre le système de router
import {BrowserRouter as Router, Switch, Route}'react-router-dom';

function App(){
    return(
        //système de router = chemin d'accès
        <Router>
            <Route path='/' exact component={Dashboard} />
            <Route path='/counters' component={Counters} /> //component = 2e route
            <Route path='/form' component={Form} />
        </Router>
        
    );
}

export default App;
````

`as` : pour renommer une librairie / fichier  
`path=' '` : chemin d'accès

Résultat = quand on écrit dans URL le nom du composant va afficher le composant (ici form va afficher la page où se trouve le composant form & counters va afficher la page où se trouve le composant counters) = départage 2 composants en 2 pages

`switch` : permet de rendre le 1er élément de match l'URL 
```javascript
    <Router>
        <Switch>
            <Route path='/' exact component={Dashboard} />
            <Route path='/couters' component={Counters} />
            <Route path='/form' component={Form} />
        </Switch>
    </Router>

//c'est la premier router qui va match avec les autres (quand écrit n'importe quel composant dans URL, va afficher le 1er (route))
```

- Ajouter un lien pour diriger dans un des composants
````javascript
//fichier Nav.js
import {Link} from 'react-router-dom';

function Nav(){
    return(
        <nav>
            <ul>
                <Link to="/"><li>Dashboard</li></Link>
                <Link to="/counters"><li>Counters</li></Link>
                <Link to="/form"><li>Form</li><link/>
                
            </ul>
        </nav>    
    )
}


export default Nev;
````

```javascript
//fichier App.js

import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';

function App(){
    return (
        <>
            <Router>
                <Nav/>
                <Switch>
                    <Route path="/"  exact component={Dashboard}></Route>
                    <Route path="/counters" component={Counters}></Route>
                    <Route path="/form" component={Form}></Route>
                </Switch>
            <Router/> 
        </>    
    );
}

export default App;
```

![router](../assets/react-router-5.png)
