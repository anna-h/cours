![react-api](../assets/react-api.png)

## AXIOS HTTP CLIENT 

[☞ Lien documentation sur axios](https://github.com/axios/axios)

![axios](../assets/react-axios.png)

    Axios = baser sur les promesses + utiliser pour réaliser des requêtes HTTP coté navigateur + serveur avec node.js

`Promoise` : ("promesse")(même principe que les intervalles) est utilisé pour réaliser des traitements de façon asynchrone, → représente une valeur qui peut être disponible maintenant / futur / jamais      
`.catch()`: catcher les erreurs

- **RÉCUPÉRATION des 'utilisateurs'**
    - JS
````javascript
//fichier Users.js
import {Component} from 'react';

import axios from 'axios';

class Users extends Component {
    
    //composant
    state = {
        users: []
    };
    
  
    //quand le composant est monté
    // ⭕️ RÉCUPÉRATION DES USERS
    componentDidMount() {
        axios.get('http://localhost:8080/users').then((response) => { //promesse "then"
            console.log('response :', response.data);
            this.setState({users: response.data});
        }); 
        this.store(); //vérification pour la méthode ".post"
    }
    
    //⭕️ CRÉER d'autre requêtes(type d'utilisateur)
    store(){
        const data = {
            firstName:"Anna",
            lastName: "Tiphanie",
            email: "news@gmail.com"
        };
        axios.post('http://localhost:8080/users', data).then((response) =>{//la méthode "post"
            console.log(response.data.message);
        }) 
    }
    
    //⭕️ SUPPRIMER (alors qu'on clique sur le bouton)
    delete(id){
        axios.delete('http://localhost:8080/users' + id).then((response) =>{ //promesse
            console.log("delete :", response)
            //récupére la liste du tableau
            const users = [...this.state.users]; //créer un nouveay tableau
            const  idx = users.findIndex((user) => user.id === id);//récupère l'index de l'utilisateur
            users.splice(idx, 1); //supprime l'index de l'utilisateur
            this.setState({users: [...users]}); //faire la mise à jour du tableau
        }).catch((err) => {
            console.log(err); //repérer les erreurs
        }); 
    }

    //return un template
    render() {
        //"users" contient un tableau de division
        const users = this.state.users.map((users) =>{
            return <div key={user.id} onClick={() => this.delete(user.id)}>{user.firstName}</div> //utilise la clé pour aider à comparer REACT du DOM //avec la méthode ".map" va fonctionner correctement meme si y a pas de données dans le tableau
        });
        return (
            <>
                {users}
            </>
        );
    }
}

export default Users;
````

```javascript
//fichier App.js (c'est un fichier pour importer)
import './App.css';
import Users from './Users/Users';

import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';

function App(){
    return(
        <>
            <Router>
                <Nav/>
                <Switch>
                    <Route path="/users" component={Users}/>
                </Switch>
            </Router>
        </>    
    );
}

export default App;
```

````javascript
//fichier Nav.js
import {Link} from 'react-router-dom';

function Nav(){
    const navStyles = {
        color: "black"
    };
    return(
        <nav>
            <ul>
                <Link style={navStyles} to="/users"><li>Users</li></Link>
            </ul>    
        </nav>    
    )
}

export default Nav;
````

❗️RECOPIER LE CODE SERVER.JS❗️ (avec les modifications)
