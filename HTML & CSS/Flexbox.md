![flexbox](../assets/flexbox-logo.png) 

        Flexbox est une méthode de mise en page selon un axe prinicipale, permettant de disposer des éléments en ligne ou en colonne 
        Est un modèle de disposition qui va nous permettre de contrôler facilement et avec précision l'alignement & la direction & l'ordre & la taille de nos éléments

[☞ Lien Guide flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)

- Valeur par défaut 
![valeur par défaut](../assets/flexbox-1.png)
  
- **DÉTERMINATION DES ÉLÉMENTS À DISPOSER EN BOITE FLEXIBLES** (`display: flex;`)

  
- **LA DIRECTION** (`flex-direction`)
    - `row` : organisés sur une ligne (par défaut)
    - `column,` : organisés sur une colonne
    - `row-reverse` : organisés sur une ligne, ordre inversé
    - `column-reverse`: organisés sur une colonne, ordre inversé


- **RETOUR À LA LIGNE**(`flex-wrap`)
    - `nowrap` : pas de retour à la ligne (par défaut)
    - `wrap` : les éléments vont à la ligne lorsqu'il n'y a plus de place
    - `wrap-reverse` : les éléments vont à la ligne, lorsqu'il n'a plus de place + sens inverse

![flex-wrap](../assets/flexbox-2.png)

- **ALIGNEMENT sur axe principal** (`justify-content`)
  - `flex-start` : alignés au début (par défaut)
  - `flex-end` : alignés à la fin
  - `center` : alignés au centre
  - `space-between` : les éléments sont étirés sur tout l'axe (espace entre eux)
  - `space-around` : les éléments sont étirés sur tout l'axe (espace sur les extrémités)
  
![justify-content](../assets/flexbox-3.png)

- **ALIGNEMENT sur axe secondaire** (`align-items`)
  - `stretch` : les éléments sont étirés sur tout l'axe (par défaut)
  - `flex-start` : alignés au début
  - `flex-end` : alignés à la fin 
  - `center`: alignés au centre
  - `baseline` : alignés sur la ligne de base (ressemble à `flex-start`)


- **RÉPARTIR PLUSIEURS LIGNES** (`align-content`)
  - `flex-start` : les éléments sont placés au début
  - `flex-end` : les éléments sont placés à la fin 
  - `center` : les éléments sont placés au centre
  - `space-between` : les éléments sont séparés (espace entre eux)
  - `space-around` : les éléments sont séparés (espace sur les extrémités)
  - `strect`(par défaut): les éléments s'étirent pour occuper tout l'espace
  
![flexbox](../assets/flexbox-4.png)
