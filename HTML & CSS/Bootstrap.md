![bootstrap-logo](../assets/bootstrap-logo.png)

    Bootstrap est un framework, dont une collection d'outils utilses à la création du design de sites & d'applications web. 
    Il contient du code HTML & CSS (=> formulaire, boutons, outils de navigation, autres éléments interactifs etc...)
    Est la bibliothèque de composants frontend la plus connue

# SOMMAIRE

- [Installation](#installation)
- [Les grilles](#les-grilles)
- [Responsive](#responsive)
- [Ajouter un composant](#ajouter-un-composant)

## ⚜️Installation⚜️    
![bootstrap](../assets/bootstrap1.png)
![boostrap](../assets/bootstrap-2.png)
    

----


## ⚜️Les grilles⚜️

    Les grilles utilise une série de lignes & de colonnes pour mettre en page le contenu

![grilles](../assets/bootstrap-3.png)

`<div>` doit déclarer un attribuant une class `row`, 1er classe de colonnes : `col` → possède une grille à 12 colonnes (row), donnant une largeur de 6 colonnes (col)

_Exemple_ :    
![bootstrap](../assets/bootstrap4.png)


----
## ⚜️Responsive⚜️

        Une page responsive est une page qui réagit à différentes tailles d'écran de l'utilisateur, en modifiant la mise en page des composants + du contenu à ajuster


⭕️ Le nombre exact de pixels de chaque tailles (écran)
![responsive](../assets/boostrap-responsive1.png)
![responsive](../assets/bootstrap-responsive.png)

- HTML 
````html
<div class="container">
   ...
   <div class="row">
      <div class="col-6 col-md-4 col-lg-3">
        Une des deux colonnes
      </div>
      <div class="col-6 col-md-8 col-lg-9">
         Une des deux colonnes
      </div>
   </div>
</div>
````

---

## ⚜️Ajouter un composant⚜️

![composant](../assets/bootstrap-composant.png)

_Exemple_ :

- HTML
```html
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="#">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Link</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Dropdown
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item" href="#">Action</a></li>
            <li><a class="dropdown-item" href="#">Another action</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="#">Something else here</a></li>
          </ul>
        </li>
        <li class="nav-item">
          <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
        </li>
      </ul>
      <form class="d-flex">
        <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success" type="submit">Search</button>
      </form>
    </div>
  </div>
</nav>
```

![composant](../assets/boostrap-composant1.png)