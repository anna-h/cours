![html&css](../assets/logo%20html%20et%20css.png)

# HTML & CSS

     HTML et CSS sont la base du fonctionnement de tous les sites web
     L'aspect et l'apparence du site

## SOMMAIRE

- [Les balises & leurs attributs](#-les-balises--leurs-attributs-)
- [HTML](#-html-)
- [Les bases du HTML](#-les-bases-du-html-)
- [CSS](#-css-)



### ⚜️ Les balises & leurs attributs ⚜️

- **Les balises** : sont invisibles à l'écran pour les navigateurs → Permet à l'ordinateur de comprendre ce qu'il doit afficher    
  `< >` : se repère facilement avec les chevrons

SYNTAXE 1
```html
<titre>Cours HTML</titre>
```

SYNTAXE 2
```html
<titre/>
```

- **Les attributs** : complémente pour donner des informations supplémentaires, se place après le nom de la balise ouvrante

```html
<balise attibut="valeur"></balise>

<image nom="photo.jpg"/>

<citation auteur="Neil Armstrong" date="21/07/1969">
    C'est un exemple
</citation>
```


## ⚜️ HTML ⚜️

     "HyperText Markup Language" ou "langage de balisage hypertexte"
     Indiquer + afficher aux navigateurs de quoi est constituée chaque page



1. Insérer des médias (images, vidéos, etc...)
2. Insérer du texte (liste, lien etc ...)

````html
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Ma page HTML</title>
</head>
<body>

<header></header>

<footer></footer>


</body>
</html>
````
        
➡️ c'est une doctype ce qui s'affichera quand on ouvre un fichier HTML = **STRUCTURE**

- `<head>` : a des informations générales sur la page (titre, l'encodage(gestion des caractères spéciaux) etc...), Ne sont pas affichées sur la page → informations générales à destination de l'ordinateur
- `<body>` : se trouve la partie principale de la page, va être affiché à l'écran (écrit la majeure partie du code)
- `<title>` : titre de la page + titre de l'onglet

**Insérer un commentaire**  :
```html
<!-- Un commentaire -->
```

### ⚜️ LES BASES DU HTML ⚜️

````html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ma page HTML</title>
</head>
<body>

<h1>Titre 1</h1>
<h2>Titre 2</h2>
<h3>Titre 3</h3>

<p>Mon texte</p>

<img src="../assets/logo%20html.png" alt="">

</body>
</html>
````

- **Titre** =  < h1 > nom < h1 > : (plus de nombre est grand plus la taille du titre rétrécit, max: 6)

```html
<h1>Titre 1</h1>

<h2>Titre 2</h2>

<h3>Titre 3</h3>

<h4>Titre 4</h4>

<h5>Titre 5</h5>

<h6>Titre 6</h6>
```

- **Paragraphe** = `<p> nom </p>`:

```html
<p>Mon texte</p>
```

- **Saut de ligne** = `<br/>`

- **En gras une selection** = `<strong>`

- **Les Listes** = 
  - Liste non ordonnée / liste à puces = `<ul></ul>`
  - Listes ordonnée / liste numérotée = `<ol></ol>`
  
```html
<ul>
  <li>Fraises</li>
  <li>Framboise</li>
  <li>Cerises</li>
</ul>
```

![listes non ordonnée](../assets/html-listes.png)

```html
<h1>Ma journée</h1>

<ol>
  <li>Je me lève</li>
  <li>Je mange et je bois</li>
  <li>Je retourne me coucher</li>
</ol>
```
![listes ordonnée](../assets/html-liste1.png)

- **Ajouter un lien** = `<a href=" "> </a>`
```html
<a href="https://gitlab.com/anna-h/cours">Mes prises de notes</a>
```

- **Lien vers une ancre** = `id=" "`, c'est un point de repère = `class`
```html
SYNTAXE 1
<a id="mes_cours">Titre</a>

SYNTAXE 2
<a href="#mes_cours"></a>
```

- **Lien pour envoyer un mail** = `mailto:`
```html
<p><a href="mailto:news@mail.com"></a></p>
```

- **Ajouter une image** = `<img scr=" "/>`
```html
<img src="image/nomdelimage.jpg" alt="facultatif">
```



-----


## ⚜️ CSS ⚜️

    "Cascading StyleSheets" "feuilles de style en cascade",  Mettre en forme en appliquant des styles la page HTML  = apparence : design, couleur, typographie, organisation

![css](../assets/css-1.png)

➡️ Il faut créer un fichier CSS `.css` + lier ce fichier avec le fichier HTML 

```html
<!doctype html>
<html lang="fr">
<head>
<meta charset="UTF-8">
             <meta charset="UTF-8"/>
            <link rel="stylesheet" href="style.css"> <!--relie le fichier.css-->
             <title>CSS</title>
</head>
<body>
  <h1>Cours de CSS</h1>
</body>
</html>
```

```css
//fichier css

p{color: red}
```

- `<div> </div>` : c'est une balise (block), entoure un bloc de texte (< p >, < h1 > etc.. ) → construction d'un design
- `<span> </span>` : c'est une balise, place au sein d'un paragraphe de texte pour sélectionner certains mots uniquement

 **L'ÉCRITURE**

`font-size` : taille en px   
`font-family` : police    
`font-style` : style (`italic`, `oblique`, `normal`)    
`font-weight`: graisse (`bold` = gras)

- **Soulignement & autres décorations** (`text-decoration`) :
  - `underline` : souligné
  - `line-trhrough` : barré
  - `overline` : ligne au-dessus
  - `blink` : clignotant
  - `none` : normal (par défaut)
  
  
- **Alignement** (`text-align`) : 
  - `left` : texte à gauche
  - `center` : texte au centre
  - `right` : texte à droite
  - `justify` : texte sera justifié





